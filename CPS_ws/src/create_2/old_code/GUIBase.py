#!/usr/bin/env python

import Tkinter as tk

class SerialPortInfo(tk.Frame):
    def __init__(self, master):
        tk.Frame.__init__(self, master, borderwidth=2, relief="groove")

        self._leftFrame = tk.Frame(self)
        self._leftFrame.pack(side="left")

        self._rightFrame = tk.Frame(self)
        self._rightFrame.pack(side="right")

        self._portLabel = tk.Label(self._leftFrame, text="serial port")
        self._portLabel.pack(side="right")

        self._portVar = tk.StringVar()
        # Note the default ttyUSB0 is for USB-to-serial adaptor.
        # For machines that use the serial port try using "/dev/ttyS0".
        self._portVar.set("/dev/ttyUSB0")
        self._portEntry = tk.Entry(self._rightFrame,
            textvariable=self._portVar)
        self._portEntry.pack(side="right")

class ConnectButton(tk.Button):
    def __init__(self, master, tick, guiClient, serialPortVar):
        tk.Button.__init__(self, master,
            text="Connect", 
            command=self.buttonCallback)
        self._guiClient = guiClient
        self._tickLabel = tick
        self._serialPortVar = serialPortVar

    def buttonCallback(self):
        self._guiClient.connect(self._serialPortVar.get())
        
        self._tickLabel.tick()

#def connect(self):
    #    connected = self._guiClient.connect(self._serialPortVar.get())
    #    if connected == True:
    #        self.config(text = "Disconnect", command = self.disconnect)
    #        self._tickLabel.tick()
    #    else:
    #        print("Connect request to %s failed"%(self._serialPortVar.get()))

    #def disconnect(self):
    #    disconnected = self._guiClient.connect(0)
    #    if disconnected == True:
    #        self.config(text = "Connect", command = self.connect)
    #    else:
    #        print ("Disconnect request to %s failed"&(self._serialPortVar.get()))

class RefreshButton(tk.Button):
    def __init__(self, master, tick, guiClient):
        tk.Button.__init__(self, master, text="check stream", command=self.buttonCallback)
        self._guiClient = guiClient
        self._tickLabel = tick

    def buttonCallback(self):
        self._tickLabel.tick()

class TickLabel(tk.Label):
    def __init__(self, master, guiClient):
        tk.Label.__init__(self, master, text="0")
        self._timeIncrement = 0
        self._guiClient = guiClient

    def tick(self):
        self._timeIncrement += 1
        self['text'] = self._timeIncrement
        #self._guiClient.getRobotState
        # serial refreshes at 15 ms intervals
        self.after(30, self.tick)

class StatusLabel(tk.Label):
    def __init__(self, master, guiClient):
        tk.Label.__init__(self, master, text="")
        #guiClient.registerSubscriber(self)
        self.buildString(guiClient.getRobotState())

    def notify(self, robotState):
        self.buildString(robotState)

class Battery(StatusLabel):
    def __init__(self, master, robotState):
        StatusLabel.__init__(self, master, robotState)

    def buildString(self, robotState):
        chargingState = robotState._battery._chargingState
        if chargingState == 0:
            state = "Not Charging"
        elif chargingState == 1:
            state = "Reconditioning Charging"
        elif chargingState == 2:
            state = "Full Charging"
        elif chargingState == 3:
            state = "Trickle Charging"
        elif chargingState == 4:
            state = "Waiting"
        elif chargingState == 5:
            state = "Charging Fault Condition"

        self["text"] = "Battery Charge %d\nBattery Capacity %d\nCharging State: %s" % (
            robotState._battery._charge,
            robotState._battery._capacity,
            state)

class Mode(StatusLabel):
    def __init__(self, master, robotState):
        StatusLabel.__init__(self, master, robotState)

    def buildString(self, robotState):
        mode = robotState._mode._oiMode
        if mode == 0:
            state = "Off"
        elif mode == 1:
            state = "Passive"
        elif mode == 2:
            state = "Safe"
        elif mode == 3:
            state = "Full"

        self["text"] = "OI Mode: %s"%(state)

class WheelVelocity(StatusLabel):
    def __init__(self, master, guiClient):
        StatusLabel.__init__(self, master, guiClient)

    def buildString(self, robotState):
        #self["text"] = "Wheel Velocity: L %d, R %d" % (
            #robotState._wheelVelocity._left, 
            #robotState._wheelVelocity._right)
        print("Wheel Velocity")

class WheelDrop(StatusLabel):
    def __init__(self, master, guiClient):
        self._timer = 0
        StatusLabel.__init__(self, master, guiClient)

    def buildString(self, robotState):
        self._timer += 1
        #self["text"] = "Wheel Drop: C %d, L %d, R %d" % (
            #robotState._wheelDrop._caster, 
            #robotState._wheelDrop._left,
            #robotState._wheelDrop._right)
        print("Wheel Drop")

class Cliff(StatusLabel):
    def __init__(self, master, guiClient):
        StatusLabel.__init__(self, master, guiClient)

    def buildString(self, robotState):
        #self["text"] = "Cliff: FL %d, FR %d, L %d, R %d" % (
            #robotState._cliff._frontLeft, 
            #robotState._cliff._frontRight,
            #robotState._cliff._left,
            #robotState._cliff._right)
        print("Cliff")

class Bumpers(StatusLabel):
    def __init__(self, master, guiClient):
        StatusLabel.__init__(self, master, guiClient)

    def buildString(self, robotState):
        #self["text"] = "Bumps: L %d, R %d" % (
            #robotState._bump._left, 
            #robotState._bump._right)
        print("Bump")

class SensorsFrame(tk.Frame):
    def __init__(self, master, guiClient):
        tk.Frame.__init__(self, master)

        self.sensorsLabel = tk.Label(self, text="Sensors")
        self.sensorsLabel.pack(side="top")

        #self.wheelVelocity = WheelVelocity(self, guiClient)
        #self.wheelVelocity.pack(side="bottom")
        
        self.battery = Battery(self, guiClient)
        self.battery.pack(side="bottom")
        
        self.mode = Mode(self, guiClient)
        self.mode.pack(side="bottom")
        
        #self.wheelDrop = WheelDrop(self, guiClient)
        #self.wheelDrop.pack(side="bottom")
        
        #self.cliff = Cliff(self, guiClient)
        #self.cliff.pack(side="bottom")
        
        #self.bumpers = Bumpers(self, guiClient)
        #self.bumpers.pack(side="bottom")

class CommandLED(tk.Frame):
    def __init__(self, master, guiClient):
        tk.Frame.__init__(self, master, borderwidth=2, relief="groove")

        self._topFrame = tk.Frame(self)
        self._topFrame.pack(side="top")

        self._bottomFrame = tk.Frame(self)
        self._bottomFrame.pack(side="bottom")

        self._guiClient = guiClient

        self._LEDBtn = tk.Button(self._topFrame,
            text="LED(whichLEDs,color,intensity)",
            command=self.buttonCallback)
        self._LEDBtn.pack(side="left")

        optionList = ("none", "play", "adv", "both")
        self._option = tk.StringVar()
        self._option.set(optionList[0])
        self._optionMenu = tk.OptionMenu(self._topFrame, self._option,
            *optionList)
        self._optionMenu.pack(side="right")

        self._colorVar = tk.StringVar()
        self._colorVar.set("0")
        self._colorEntry = tk.Entry(self._bottomFrame, 
            textvariable=self._colorVar)
        self._colorEntry.pack(side="left")

        self._intensityVar = tk.StringVar()
        self._intensityVar.set("0")
        self._intensityEntry = tk.Entry(self._bottomFrame, 
            textvariable=self._intensityVar)
        self._intensityEntry.pack(side="left")

    def buttonCallback(self):
        optString = self._option.get()
        whichLED = 0 # none
        if (optString == "play"):
            whichLED = 2
        elif (optString == "adv"):
            whichLED = 8
        elif (optString == "both"):
            whichLED = 2 | 8 # both
        color = int(self._colorVar.get())
        intensity = int(self._intensityVar.get())
        self._guiClient.setLEDs(whichLED, color, intensity)

class CommandDrive(tk.Frame):
    def __init__(self, master, guiClient):
        tk.Frame.__init__(self, master, borderwidth=2, relief="groove")

        self._topFrame = tk.Frame(self)
        self._topFrame.pack(side="top")


        self._bottomFrame = tk.Frame(self)
        self._bottomFrame.pack(side="bottom")

        self._guiClient = guiClient

        self._driveBtn = tk.Button(self._topFrame,
            text="drive(velocity,radius)",
            command=self.buttonCallback)
        self._driveBtn.pack(side="left")

        self._velocityVar = tk.StringVar()
        self._velocityVar.set("0")
        self._velocityEntry = tk.Entry(self._bottomFrame, 
            textvariable=self._velocityVar)
        self._velocityEntry.pack(side="left")

        self._radiusVar = tk.StringVar()
        self._radiusVar.set("0")
        self._radiusEntry = tk.Entry(self._bottomFrame, 
            textvariable=self._radiusVar)
        self._radiusEntry.pack(side="left")

    def buttonCallback(self):
        velocity = int(self._velocityVar.get())
        radius = int(self._radiusVar.get())
        self._guiClient.drive(velocity, radius)

class CommandDemo(tk.Frame):
    def __init__(self, master, guiClient):
        tk.Frame.__init__(self, master, borderwidth=2, relief="groove")

        self._topFrame = tk.Frame(self)
        self._topFrame.pack(side="top")

        self._bottomFrame = tk.Frame(self)
        self._bottomFrame.pack(side="bottom")

        self._guiClient = guiClient

        self._demoBtn = tk.Button(self._topFrame,
            text="demo(option)",
            command=self.buttonCallback)
        self._demoBtn.pack(side="left")

        optionList = ("abort", "max clean", "clean", 
            "cover and dock", "spot cover")
        self._option = tk.StringVar()
        self._option.set(optionList[0])
        self._optionMenu = tk.OptionMenu(self._topFrame, self._option,
            *optionList)
        self._optionMenu.pack(side="right")
#Updated for Create 2.  Demo was removed so individual Opcodes were needed.
    def buttonCallback(self):  
#        opt = 255 # none
        self._guiClient.demo(self._option.get())

class CommandDrive(tk.Frame):
    def __init__(self, master, guiClient):
        tk.Frame.__init__(self, master, borderwidth=2, relief="groove")

        self._topFrame = tk.Frame(self)
        self._topFrame.pack(side="top")

        self._bottomFrame = tk.Frame(self)
        self._bottomFrame.pack(side="bottom")

        self._guiClient = guiClient

        self._driveBtn = tk.Button(self._topFrame,
            text="drive(velocity,radius)",
            command=self.buttonCallback)
        self._driveBtn.pack(side="left")

        self._velocityVar = tk.StringVar()
        self._velocityVar.set("0")
        self._velocityEntry = tk.Entry(self._bottomFrame, 
            textvariable=self._velocityVar)
        self._velocityEntry.pack(side="left")

        self._radiusVar = tk.StringVar()
        self._radiusVar.set("0")
        self._radiusEntry = tk.Entry(self._bottomFrame, 
            textvariable=self._radiusVar)
        self._radiusEntry.pack(side="left")

    def buttonCallback(self):
        velocity = int(self._velocityVar.get())
        radius = int(self._radiusVar.get())
        self._guiClient.drive(velocity, radius)

class CommandDriveDirect(tk.Frame):
    def __init__(self, master, guiClient):
        tk.Frame.__init__(self, master, borderwidth=2, relief="groove")

        self._topFrame = tk.Frame(self)
        self._topFrame.pack(side="top")

        self._bottomFrame = tk.Frame(self)
        self._bottomFrame.pack(side="bottom")

        self._guiClient = guiClient

        self._driveBtn = tk.Button(self._topFrame,
            text="driveDirect(right velocity,left velocity)",
            command=self.buttonCallback)
        self._driveBtn.pack(side="left")

        self._rightVelocityVar = tk.StringVar()
        self._rightVelocityVar.set("0")
        self._rightVelocityEntry = tk.Entry(self._bottomFrame, 
            textvariable=self._rightVelocityVar)
        self._rightVelocityEntry.pack(side="left")

        self._leftVelocityVar = tk.StringVar()
        self._leftVelocityVar.set("0")
        self._leftVelocityEntry = tk.Entry(self._bottomFrame, 
            textvariable=self._leftVelocityVar)
        self._leftVelocityEntry.pack(side="left")

    def buttonCallback(self):
        rightVelocity = int(self._rightVelocityVar.get())
        leftVelocity = int(self._leftVelocityVar.get())
        self._guiClient.driveDirect(rightVelocity, leftVelocity)

class CommandStopMoving(tk.Button):
    def __init__(self, master, guiClient):
        tk.Button.__init__(self, master,
            text="Stop Moving",
            command=self.buttonCallback)
        self._guiClient = guiClient

    def buttonCallback(self):
        self._guiClient.driveDirect(0, 0)

class testButton(tk.Button):
    def __init__(self, master, guiClient):
        tk.Button.__init__(self, master,
            text="Test service", 
            command=self.buttonCallback)
        self._guiClient = guiClient

    def buttonCallback(self):
        self._guiClient.testSend()

class ClientApp(tk.Frame):
    def __init__(self, guiClient, master=None):
        tk.Frame.__init__(self, master)
        self.pack()

        self._guiClient = guiClient

        self._connectFrame = tk.Frame(self)
        self._connectFrame.pack(side="top", fill='x')

        self._stop = CommandStopMoving(self, self._guiClient)
        self._stop.pack(side="bottom", fill='x')

        modeList = ["off", "passive", "safe", "full"]
        self.defaultMode = tk.StringVar()
        self.defaultMode.set(modeList[0])
        self._modeMenu = tk.OptionMenu(self._connectFrame, self.defaultMode, *modeList, command=self.setMode)
        self._modeMenu.pack(side="top")

        # Power Frame

        self._powerFrame = tk.Frame(self)
        self._powerFrame.pack(side="bottom", fill='x')

        self._startCommand = tk.Button(self._powerFrame, text="Start", command=lambda:self.powerCommand("start"))
        self._startCommand.pack(side="left")

        self._stopCommand = tk.Button(self._powerFrame, text="Power Off", command=lambda: self.powerCommand("stop"))
        self._stopCommand.pack(side="left")

        self._resetCommand = tk.Button(self._powerFrame, text="Reset", command=lambda: self.powerCommand("reset"))
        self._resetCommand.pack(side="left")

        self._tick = TickLabel(self, self._guiClient)
        self._tick.pack(side="bottom")

        self._serialPortInfo = SerialPortInfo(self._connectFrame)
        self._connect = ConnectButton(self._connectFrame, self._tick, 
            self._guiClient, self._serialPortInfo._portVar)

        self._connect.pack()
        self._serialPortInfo.pack()

        self._commandLED = CommandLED(self, self._guiClient)
        self._commandLED.pack(side="bottom")

        self._commandDemo = CommandDemo(self, self._guiClient)
        self._commandDemo.pack(side="bottom")

        self._commandDrive = CommandDrive(self, self._guiClient)
        self._commandDrive.pack(side="bottom")

        self._commandDrive = CommandDriveDirect(self, self._guiClient)
        self._commandDrive.pack(side="bottom")

        self._sensorsFrame = SensorsFrame(self, 
            self._guiClient)
        self._sensorsFrame.pack(side="bottom")

        self._testSend = testButton(self, self._guiClient)
        self._testSend.pack(side="bottom")

        self._resetModel = tk.Button(self, text="Reset Model", command = self.resetModel)
        self._resetModel.pack(side="bottom")

    def stateChange(self, robotState):
        print("stateChange")

    # Start robot, power it off, or restart it
    def powerCommand(self, command):
        self._guiClient.powerCommand(command)

    # Set mode to Off, Passive, Safe, or Full
    def setMode(self, mode):
        self._guiClient.setMode(mode)

    # Reset Gazebo model
    def resetModel(self):
        self._guiClient.resetModel()

    def updateGUI(self):
        return

def updateCallback(robotState):
    app.stateChange(robotState)
