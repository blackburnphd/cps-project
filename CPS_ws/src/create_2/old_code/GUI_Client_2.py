#!/usr/bin/env python
import rospy
import nodes
from std_msgs.msg import String
from create_2.srv import *
from create_2.msg import IntList
from gazebo_msgs.msg import ModelState
from gazebo_msgs.msg import ModelStates

class GUI_Client:
    def __init__(self, node_name):
        rospy.init_node(node_name)
        self._robotState = ""
        rospy.Subscriber('robotState', IntList, self.getRobotState)
        rospy.loginfo("GUI Client is ready")
        #rospy.spin()     

    def connect(self, serialPortVar):
        #Code for service request
        try:
            # Call connect service
            connect = rospy.ServiceProxy('connect', Connect)
            respl = connect(serialPortVar)
            rospy.loginfo("Successfully queried connection service to port: %s"%(serialPortVar))
            return True
        except rospy.ServiceException as e:
            rospy.logerr("Connection service query to port %s has failed: %s"%(serialPortVar, e))
            return False

    # Get robotState message
    def getRobotState(self, msg):
        print("Name:", msg.name)
        print("Data:", msg.data)
        #rospy.loginfo("Receiving robotState information")
        #Set robotState information to received data

    def setLEDs(self, whichLED, color, intensity):
        #robotproxy.setleds
        try:
            # Call setLEDs service
            setLEDs = rospy.ServiceProxy('setLEDs', SetLEDs)
            respl = setLEDs(whichLED, color, intensity)
            rospy.loginfo("Successfully queried setLEDs service: (%s,%s,%s)"%(whichLED, color, intensity))
        except rospy.ServiceException as e:
            rospy.logerr("setLEDs service (%s,%s,%s) query has failed: %s"%(whichLED, color, intensity, e))

    def drive(self, velocity, radius):
        rospy.wait_for_service('drive')
        try:
            # Call drive service
            drive = rospy.ServiceProxy('drive', Drive)
            respl = drive(velocity, radius)
            rospy.loginfo("Successfully queried drive service: (%s,%s)"%(velocity, radius))
        except rospy.ServiceException as e:
            rospy.logerr("drive service (%s,%s) query has failed: %s"%(velocity, radius, e))

    def demo(self, demoType):
        rospy.wait_for_service('demo')
        try:
            # Call demo service
            demo = rospy.ServiceProxy('demo', Demo)
            respl = demo(demoType)
            rospy.loginfo("Successfully queried demo service: %s"%(demoType))
        except rospy.ServiceException as e:
            rospy.logerr("demo service %s query has failed: %s"%(demoType, e))
        #max clean, clean, cover and dock, spot, start

    def driveDirect(self, rightVelocity, leftVelocity):
        #robotproxy.drivedirect
        rospy.wait_for_service('driveDirect')
        try:
            # Call driveDirect service
            driveDirect = rospy.ServiceProxy('driveDirect', DriveDirect)
            driveDirect(rightVelocity, leftVelocity)
            rospy.loginfo("Successfully queried driveDirect service: (%s, %s)"%(rightVelocity, leftVelocity))
        except rospy.ServiceException as e:
            rospy.logerr("driveDirect service query has failed: %s"%(e))

    def testSend(self):
        rospy.wait_for_service('testService')
        try:
            # Call test service
            print("making service proxy")
            test = rospy.ServiceProxy('testService', Test)
            print("invoking test(69)")
            respl = test(69)
            print("return result%s"%(respl.resp))
            return respl.resp
        except rospy.ServiceException as e:
            print("Test service call failed: %s"%e)
            return False

    # Set robot mode
    def setMode(self, mode):
        rospy.wait_for_service('setMode')
        try:
            # Call mode service
            modeService = rospy.ServiceProxy('setMode', SetMode)
            respl = modeService(mode)
            rospy.loginfo("Successfully queried mode service: %s"%(mode))
        except rospy.ServiceException as e:
            rospy.logerr("mode service %s query has failed: %s"%(mode, e))

    def resetModel(self):
        #Dan implement model reset here
        return
