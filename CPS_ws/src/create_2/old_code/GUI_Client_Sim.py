#!/usr/bin/env python
import rospy
import nodes
from std_msgs.msg import String
from create_2.srv import *
from create_2.msg import IntList
from gazebo_msgs.msg import ModelState
from gazebo_msgs.msg import ModelStates

class GUI_Client:
    def __init__(self, node_name):
        rospy.init_node(node_name)
        self._robotState = ""
        rospy.Subscriber('robotState', IntList, self.getRobotState)
        rospy.loginfo("GUI Client is ready")
        #rospy.spin()     

    # Get robotState message
    def getRobotState(self, msg):
        print("Name:", msg.name)
        print("Data:", msg.data)
        #rospy.loginfo("Receiving robotState information")
        #Set robotState information to received data

    def drive(self, velocity, radius):
        rospy.wait_for_service('drive')
        try:
            # Call drive function
            nodes.driveRobotNode(velocity, radius)
        except rospy.ServiceException as e:
            rospy.logerr("drive service (%s,%s) query has failed: %s"%(velocity, radius, e))

    def demo(self, demoType):
        rospy.wait_for_service('demo')
        try:
            # Call demo service
            demo = rospy.ServiceProxy('demo', Demo)
            respl = demo(demoType)
            rospy.loginfo("Successfully queried demo service: %s"%(demoType))
        except rospy.ServiceException as e:
            rospy.logerr("demo service %s query has failed: %s"%(demoType, e))
        #max clean, clean, cover and dock, spot, start

    def driveDirect(self, rightVelocity, leftVelocity):
        #robotproxy.drivedirect
        rospy.wait_for_service('driveDirect')
        try:
            # Call driveDirect function
            nodes.driveWheelsNode(rightVelocity, leftVelocity)
        except rospy.ServiceException as e:
            rospy.logerr("driveDirect service query has failed: %s"%(e))

    def resetModel(self):
        nodes.reset("create")
        return
