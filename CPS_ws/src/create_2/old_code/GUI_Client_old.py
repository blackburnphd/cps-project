#!/usr/bin/env python
import Tkinter as tk
import GUIBase_Node as gb
import GUI_Client as gc
import rospy
import nodes
from std_msgs.msg import String
#from create2.srv import *
from gazebo_msgs.msg import ModelState
from gazebo_msgs.msg import ModelStates

class GUI_Client:
    def __init__(self):
        rospy.init_node('gui_client')
        # nodes.spawnCreate()
        #self.connect_pub = rospy.Publisher('connect', String, queue_size=10)
        #self.startStream_pub = rospy.Publisher('startStream', int64, queue_size=10)
        rate = rospy.Rate(10)
        self._robotState = ""
        #Do I need to start a service or something?
        print("Made a GUI_Client")
        #Maybe use a client side robotState to represent the robot?  
        # nodes.teleopDriveRobot()      

    def getRobotState(self):
        #Check robotState
        #Return robotstate
        try:
            # Call get robot state service
            getRobotState = rospy.ServiceProxy('getRobotState', GetRobotState)
            self._robotState = getRobotState()
            return True
        except rospy.ServiceException as e:
            print("Get Robot State service call failed: %s"%e)
            return False

    def drive(self, velocity, radius):
        #rospy.wait_for_service('drive')
        try:
            # Call drive service
            #drive = rospy.ServiceProxy('drive', Drive)
            #respl = drive(velocity, radius)
            if (velocity == radius == 0):
                nodes.reset("create")
            else:
                nodes.driveRobotNode(velocity, radius)
            # nodes.driveRobotService(velocity, radius)
            #return True
        except rospy.ServiceException as e:
            print("Drive service call failed: %s"%e)
            return False

    def demo(self, demoType):
        rospy.wait_for_service('demo')
        try:
            # Call demo service
            demo = rospy.ServiceProxy('demo', Demo)
            respl = demo(demoType)
            return True
        except rospy.ServiceException as e:
            print("Demo service call failed: %s"%e)
            return False
        #max clean, clean, cover and dock, spot, start

    def driveDirect(self, rightVelocity, leftVelocity):
        #robotproxy.drivedirect
        #rospy.wait_for_service('driveDirect')
        try:
            # Call drive service
            #driveDirect = rospy.ServiceProxy('driveDirect', DriveDirect)
            #respl = driveDirect(rightVelocity, leftVelocity)
            #driveRobotNode(rightVelocity, leftVelocity)
            nodes.driveWheelsNode(rightVelocity, leftVelocity)
            return True
        except rospy.ServiceException as e:
            print("Drive Direct service call failed: %s"%e)
            return False

    def testSend(self):
        rospy.wait_for_service('testService')
        try:
            # Call test service
            print("making service proxy")
            test = rospy.ServiceProxy('testService', Test)
            print("invoking test(69)")
            respl = test(69)
            print("return result%s"%(respl.resp))
            return respl.resp
        except rospy.ServiceException as e:
            print("Test service call failed: %s"%e)
            return False

    def reset(self):
        nodes.reset("create")
