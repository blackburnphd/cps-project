import tkinter as tk

class SerialPortInfo(tk.Frame):
    def __init__(self, master):
        tk.Frame.__init__(self, master, borderwidth=2, relief="groove")

        self._leftFrame = tk.Frame(self)
        self._leftFrame.pack(side="left")

        self._rightFrame = tk.Frame(self)
        self._rightFrame.pack(side="right")

        self._portLabel = tk.Label(self._leftFrame, text="serial port")
        self._portLabel.pack(side="right")

        self._portVar = tk.StringVar()
        # Note the default ttyUSB0 is for USB-to-serial adaptor.
        # For machines that use the serial port try using "/dev/ttyS0".
        self._portVar.set("/dev/ttyUSB0")
        self._portEntry = tk.Entry(self._rightFrame,
            textvariable=self._portVar)
        self._portEntry.pack(side="right")

class ConnectButton(tk.Button):
    def __init__(self, master, tick, robotProxy, serialPortVar):
        tk.Button.__init__(self, master,
            text="start stream", 
            command=self.buttonCallback)
        self._robotProxy = robotProxy
        self._tickLabel = tick
        self._serialPortVar = serialPortVar

    def buttonCallback(self):
        self._robotProxy.setSerialPortInfo(self._serialPortVar.get())
        self._robotProxy.connect()
        self._robotProxy.startStream()
        self._tickLabel.tick()

class RefreshButton(tk.Button):
    def __init__(self, master, tick, robotProxy):
        tk.Button.__init__(self, master,
            text="check stream", 
            command=self.buttonCallback)
        self._robotProxy = robotProxy
        self._tickLabel = tick

    def buttonCallback(self):
        self._tickLabel.tick()

class TickLabel(tk.Label):
    def __init__(self, master, robotProxy):
        tk.Label.__init__(self, master, text="0")
        self._timeIncrement = 0
        self._robotProxy = robotProxy

    def tick(self):
        self._timeIncrement += 1
        self['text'] = self._timeIncrement
        self._robotProxy.checkStream()
        # serial refreshes at 15 ms intervals
        self.after(30, self.tick)

class StatusLabel(tk.Label):
    def __init__(self, master, robotProxy):
        tk.Label.__init__(self, master, text="")
        robotProxy.registerSubscriber(self)
        self.buildString(robotProxy._robotState)

    def notify(self, robotState):
        self.buildString(robotState)

class Battery(StatusLabel):
    def __init__(self, master, robotState):
        StatusLabel.__init__(self, master, robotState)

    def buildString(self, robotState):
        self["text"] = "battery charge %d, capacity %d" % (
            robotState._battery._charge,
            robotState._battery._capacity)

class WheelVelocity(StatusLabel):
    def __init__(self, master, robotProxy):
        StatusLabel.__init__(self, master, robotProxy)

    def buildString(self, robotState):
        self["text"] = "Wheel Velocity: L %d, R %d" % (
            robotState._wheelVelocity._left, 
            robotState._wheelVelocity._right)

class WheelDrop(StatusLabel):
    def __init__(self, master, robotProxy):
        self._timer = 0
        StatusLabel.__init__(self, master, robotProxy)

    def buildString(self, robotState):
        self._timer += 1
        self["text"] = "Wheel Drop: C %d, L %d, R %d" % (
            robotState._wheelDrop._caster, 
            robotState._wheelDrop._left,
            robotState._wheelDrop._right)

class Cliff(StatusLabel):
    def __init__(self, master, robotProxy):
        StatusLabel.__init__(self, master, robotProxy)

    def buildString(self, robotState):
        self["text"] = "Cliff: FL %d, FR %d, L %d, R %d" % (
            robotState._cliff._frontLeft, 
            robotState._cliff._frontRight,
            robotState._cliff._left,
            robotState._cliff._right)

class Bumpers(StatusLabel):
    def __init__(self, master, robotProxy):
        StatusLabel.__init__(self, master, robotProxy)

    def buildString(self, robotState):
        self["text"] = "Bumps: L %d, R %d" % (
            robotState._bump._left, 
            robotState._bump._right)

class SensorsFrame(tk.Frame):
    def __init__(self, master, robotProxy):
        tk.Frame.__init__(self, master)

        self.sensorsLabel = tk.Label(self, text="Sensors")
        self.sensorsLabel.pack(side="top")

        self.wheelVelocity = WheelVelocity(self, robotProxy)
        self.wheelVelocity.pack(side="bottom")
        
        self.battery = Battery(self, robotProxy)
        self.battery.pack(side="bottom")
        
        self.wheelDrop = WheelDrop(self, robotProxy)
        self.wheelDrop.pack(side="bottom")
        
        self.cliff = Cliff(self, robotProxy)
        self.cliff.pack(side="bottom")
        
        self.bumpers = Bumpers(self, robotProxy)
        self.bumpers.pack(side="bottom")

class CommandLED(tk.Frame):
    def __init__(self, master, robotProxy):
        tk.Frame.__init__(self, master, borderwidth=2, relief="groove")

        self._topFrame = tk.Frame(self)
        self._topFrame.pack(side="top")

        self._bottomFrame = tk.Frame(self)
        self._bottomFrame.pack(side="bottom")

        self._robotProxy = robotProxy

        self._LEDBtn = tk.Button(self._topFrame,
            text="LED(whichLEDs,color,intensity)",
            command=self.buttonCallback)
        self._LEDBtn.pack(side="left")

        optionList = ("none", "play", "adv", "both")
        self._option = tk.StringVar()
        self._option.set(optionList[0])
        self._optionMenu = tk.OptionMenu(self._topFrame, self._option,
            *optionList)
        self._optionMenu.pack(side="right")

        self._colorVar = tk.StringVar()
        self._colorVar.set("0")
        self._colorEntry = tk.Entry(self._bottomFrame, 
            textvariable=self._colorVar)
        self._colorEntry.pack(side="left")

        self._intensityVar = tk.StringVar()
        self._intensityVar.set("0")
        self._intensityEntry = tk.Entry(self._bottomFrame, 
            textvariable=self._intensityVar)
        self._intensityEntry.pack(side="left")

    def buttonCallback(self):
        optString = self._option.get()
        whichLED = 0 # none
        if (optString == "play"):
            whichLED = 2
        elif (optString == "adv"):
            whichLED = 8
        elif (optString == "both"):
            whichLED = 2 | 8 # both
        color = int(self._colorVar.get())
        intensity = int(self._intensityVar.get())
        self._robotProxy.setLEDs(whichLED, color, intensity)

class CommandDrive(tk.Frame):
    def __init__(self, master, robotProxy):
        tk.Frame.__init__(self, master, borderwidth=2, relief="groove")

        self._topFrame = tk.Frame(self)
        self._topFrame.pack(side="top")

        self._bottomFrame = tk.Frame(self)
        self._bottomFrame.pack(side="bottom")

        self._robotProxy = robotProxy

        self._driveBtn = tk.Button(self._topFrame,
            text="drive(velocity,radius)",
            command=self.buttonCallback)
        self._driveBtn.pack(side="left")

        self._velocityVar = tk.StringVar()
        self._velocityVar.set("0")
        self._velocityEntry = tk.Entry(self._bottomFrame, 
            textvariable=self._velocityVar)
        self._velocityEntry.pack(side="left")

        self._radiusVar = tk.StringVar()
        self._radiusVar.set("0")
        self._radiusEntry = tk.Entry(self._bottomFrame, 
            textvariable=self._radiusVar)
        self._radiusEntry.pack(side="left")

    def buttonCallback(self):
        velocity = int(self._velocityVar.get())
        radius = int(self._radiusVar.get())
        self._robotProxy.drive(velocity, radius)

class CommandDemo(tk.Frame):
    def __init__(self, master, robotProxy):
        tk.Frame.__init__(self, master, borderwidth=2, relief="groove")

        self._topFrame = tk.Frame(self)
        self._topFrame.pack(side="top")

        self._bottomFrame = tk.Frame(self)
        self._bottomFrame.pack(side="bottom")

        self._robotProxy = robotProxy

        self._demoBtn = tk.Button(self._topFrame,
            text="demo(option)",
            command=self.buttonCallback)
        self._demoBtn.pack(side="left")

        optionList = ("abort", "cover", 
            "cover and dock", "spot cover",
            "mouse", "figure eight", "wimp", "home",
            "tag", "pachelbel", "banjo")
        self._option = tk.StringVar()
        self._option.set(optionList[0])
        self._optionMenu = tk.OptionMenu(self._topFrame, self._option,
            *optionList)
        self._optionMenu.pack(side="right")

    def buttonCallback(self):
        optString = self._option.get()
        opt = 255 # none
        if (optString == "abort"):
            opt = -1
        elif (optString == "cover"):
            opt = 0
        elif (optString == "cover and dock"):
            opt = 1
        elif (optString == "spot cover"):
            opt = 2
        elif (optString == "mouse"):
            opt = 3
        elif (optString == "figure eight"):
            opt = 4
        elif (optString == "wimp"):
            opt = 5
        elif (optString == "home"):
            opt = 6
        elif (optString == "tag"):
            opt = 7
        elif (optString == "pachelbel"):
            opt = 8
        elif (optString == "banjo"):
            opt = 9
        self._robotProxy.demo(opt)

class CommandDrive(tk.Frame):
    def __init__(self, master, robotProxy):
        tk.Frame.__init__(self, master, borderwidth=2, relief="groove")

        self._topFrame = tk.Frame(self)
        self._topFrame.pack(side="top")

        self._bottomFrame = tk.Frame(self)
        self._bottomFrame.pack(side="bottom")

        self._robotProxy = robotProxy

        self._driveBtn = tk.Button(self._topFrame,
            text="drive(velocity,radius)",
            command=self.buttonCallback)
        self._driveBtn.pack(side="left")

        self._velocityVar = tk.StringVar()
        self._velocityVar.set("0")
        self._velocityEntry = tk.Entry(self._bottomFrame, 
            textvariable=self._velocityVar)
        self._velocityEntry.pack(side="left")

        self._radiusVar = tk.StringVar()
        self._radiusVar.set("0")
        self._radiusEntry = tk.Entry(self._bottomFrame, 
            textvariable=self._radiusVar)
        self._radiusEntry.pack(side="left")

    def buttonCallback(self):
        velocity = int(self._velocityVar.get())
        radius = int(self._radiusVar.get())
        self._robotProxy.drive(velocity, radius)

class CommandDriveDirect(tk.Frame):
    def __init__(self, master, robotProxy):
        tk.Frame.__init__(self, master, borderwidth=2, relief="groove")

        self._topFrame = tk.Frame(self)
        self._topFrame.pack(side="top")

        self._bottomFrame = tk.Frame(self)
        self._bottomFrame.pack(side="bottom")

        self._robotProxy = robotProxy

        self._driveBtn = tk.Button(self._topFrame,
            text="driveDirect(right velocity,left velocity)",
            command=self.buttonCallback)
        self._driveBtn.pack(side="left")

        self._rightVelocityVar = tk.StringVar()
        self._rightVelocityVar.set("0")
        self._rightVelocityEntry = tk.Entry(self._bottomFrame, 
            textvariable=self._rightVelocityVar)
        self._rightVelocityEntry.pack(side="left")

        self._leftVelocityVar = tk.StringVar()
        self._leftVelocityVar.set("0")
        self._leftVelocityEntry = tk.Entry(self._bottomFrame, 
            textvariable=self._leftVelocityVar)
        self._leftVelocityEntry.pack(side="left")

    def buttonCallback(self):
        rightVelocity = int(self._rightVelocityVar.get())
        leftVelocity = int(self._leftVelocityVar.get())
        self._robotProxy.driveDirect(rightVelocity, leftVelocity)

class ClientApp(tk.Frame):

    def __init__(self, robotProxy, master=None):
        tk.Frame.__init__(self, master)
        self.pack()

        self._robotProxy = robotProxy

        self._tick = TickLabel(self, self._robotProxy)
        self._tick.pack(side="bottom")

        self._serialPortInfo = SerialPortInfo(self)

        self._connect = ConnectButton(self, self._tick, 
            self._robotProxy, self._serialPortInfo._portVar)

        self._connect.pack(side="top")
        self._serialPortInfo.pack(side="top")

        self._commandLED = CommandLED(self, self._robotProxy)
        self._commandLED.pack(side="bottom")

        self._commandDemo = CommandDemo(self, self._robotProxy)
        self._commandDemo.pack(side="bottom")

        self._commandDrive = CommandDrive(self, self._robotProxy)
        self._commandDrive.pack(side="bottom")

        self._commandDrive = CommandDriveDirect(self, self._robotProxy)
        self._commandDrive.pack(side="bottom")

        self._sensorsFrame = SensorsFrame(self, 
            self._robotProxy)
        self._sensorsFrame.pack(side="bottom")

    def stateChange(self, robotState):
        print("stateChange")

def updateCallback(robotState):
    app.stateChange(robotState)
