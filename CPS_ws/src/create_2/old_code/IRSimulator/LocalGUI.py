import tkinter as tk
import GUIBase as gb
import LocalRobotProxy as lrp

robotProxy = lrp.LocalRobotProxy()
root = tk.Tk()
app = gb.ClientApp(robotProxy, master=root)

root.mainloop()
