'''
Created on Oct 12, 2015

@author: cjhoel
'''

class Monitor:
    '''
    classdocs
    '''


    def __init__(self,proxy):
        self._robotProxy=proxy
        '''
        Constructor
        '''
    def read_Wheelspeed(self):
        #1.1.1.6 #1.2.1.6
        L_Wheelspeed = self._robotProxy._robotState._wheelVelocity._left
        R_Wheelspeed = self._robotProxy._robotState._wheelVelocity._right
        O_Wheelspeed = self._robotProxy._robotState._wheelVelocity._overall
        wheelspeed = [L_Wheelspeed,R_Wheelspeed,O_Wheelspeed]
        return(wheelspeed)
    
    def limit_wheelspeed_direct(self,l_wheelspeed_raw, r_wheelspeed_raw, wheelspeed_limit):
        if r_wheelspeed_raw != 0 and l_wheelspeed_raw != 0:
            r_sign = int(r_wheelspeed_raw)/abs(int(r_wheelspeed_raw))
            l_sign = int(l_wheelspeed_raw)/abs(int(l_wheelspeed_raw))
        else:
            r_sign = 1
            l_sign = 1
            
        if r_sign == 1:
            p1 = min(r_wheelspeed_raw,wheelspeed_limit) #1.1.2.1 #1.2.3.4
        elif r_sign == -1:
            p1 = max(r_wheelspeed_raw,-wheelspeed_limit) #1.1.2.1 #1.2.3.4
        if l_sign == 1:
            p2 = min(l_wheelspeed_raw,wheelspeed_limit) #1.1.2.1 #1.2.3.4
        elif l_sign == -1:
            p2 = max(l_wheelspeed_raw,-wheelspeed_limit) #1.1.2.1 #1.2.3.4
        if p1 != r_wheelspeed_raw or p2 != l_wheelspeed_raw:
            if abs(r_wheelspeed_raw) > abs(l_wheelspeed_raw):
                r_wheelspeed_final = p1
                l_wheelspeed_final = p2*abs(l_wheelspeed_raw/r_wheelspeed_raw)
            elif abs(r_wheelspeed_raw) < abs(l_wheelspeed_raw):
                l_wheelspeed_final = p2
                r_wheelspeed_final = p1*abs(r_wheelspeed_raw/l_wheelspeed_raw)
            else: 
                r_wheelspeed_final = p1
                l_wheelspeed_final = p2
        else:
            r_wheelspeed_final = p1
            l_wheelspeed_final = p2 
        return(r_wheelspeed_final,l_wheelspeed_final)  

    def monitor_handle_socket(self,ready,conn, wheelspeed_limit):
        #1.1.1.6 #1.2.1.7
        self._conn = conn
        print('ready= ', ready[0])
        if (ready[0]):
            data = self._conn.recv(1024).strip()
            print(data)
            if not data:
                print('No command')
                return True
            else:
                # convert bytes to string
                dataString = data.decode("utf-8")
                print("received: ", dataString)

                # convert string to list of tokens
                tokens = dataString.split()
                if (tokens[0] == "startStream"):
                    serialPortInfo = tokens[1]
                    self._robotProxy.setSerialPortInfo(serialPortInfo)
                    self._robotProxy.connect()
                    self._robotProxy.startStream()
                elif (tokens[0] == "drive"):
                    p1 = int(tokens[1])
                    p2 = int(tokens[2])
                    p1 = min(p1,wheelspeed_limit) #1.1.2.1 #1.2.3.4
                    self._robotProxy.drive(p1, p2) #1.2.2.1
                elif (tokens[0] == "driveDirect"):
                    wheelspeed = self.limit_wheelspeed_direct(int(tokens[2]),int(tokens[1]),wheelspeed_limit)
                    self._robotProxy.driveDirect(wheelspeed[0], wheelspeed[1]) #1.2.2.1
                    
    def read_sensor_vld(self, sensor_data, status_history, ready, conn):
        wheelspeed_limit = 50 #mm/sec
        voltage_min = 0  #1.2.1.2
        voltage_max = 999 #1.2.1.2
        print(sensor_data)
        sensor_voltage = float(sensor_data[0]) #1.2.1.1
        sensor_valid = int(sensor_data[1]) #1.2.1.4
        checksum = str(sensor_data[2]) #1.2.3.5
        heartbeat = int(sensor_data[3]) #1.2.1.3
        
        valid_status = status_history[1]
        valid_persistence = status_history[2]
        last_heartbeat = status_history[3]
        
        #Validity persistence
        #1.2.3.1
        if valid_status == 1:
            if sensor_valid==1:
                valid_persistence = 0
                print("valid data")
            else:
                valid_persistence +=1
                if valid_persistence >= 3:
                    valid_status = 0
                    valid_persistence = 0
                    print('Invalid Data')
                else:
                    print('Sensor Valid with ' + str(valid_persistence) + ' frames of bad data')
                
        else:
            if sensor_valid==1:
                valid_persistence +=1
                if valid_persistence >= 3:
                    valid_status = 1
                    valid_persistence = 0
                    print('Valid Data')                    
                else:
                    print('Sensor Invalid with ' + str(valid_persistence) + ' frames of good data')
            else:
                valid_persistence = 0
                print("Invalid data")
        #Checksum no persistence because the values are stored in RAM so transmission anomalies do not need to be accounted for
        #1.2.3.1
        if checksum== 'good':
            checksum_status = 1
            print("Good Checksum")
        else:
            checksum_status = 0
            print("Bad Checksum")

        #Heartbeat
        #1.2.3.1
        if heartbeat!=last_heartbeat:
            heartbeat_status = 1
            last_heartbeat = heartbeat
            print("Fresh Heartbeat")
        else:
            heartbeat_status = 0
            last_heartbeat = heartbeat
            print("Heartbeat stale")
        
        #Voltage Range
        #1.2.3.1
        if voltage_min<sensor_voltage<=voltage_max:
            voltage_status = 1
            print("Valid Voltage")
        else:
            voltage_status = 0
            print("Voltage out of range")
        
        all_valid = valid_status*checksum_status*heartbeat_status*voltage_status
        if all_valid==1:
            #1.2.3.3
            print("All Valid")
        else:
            print("Invalid Collision Sensor. Manage drive commands.")
            #1.2.3.2
            self._robotProxy.checkStream()
            wheelspeed = self.read_Wheelspeed()
            wheelspeed_cmd = self.limit_wheelspeed_direct(wheelspeed[0],wheelspeed[1],wheelspeed_limit)
            if wheelspeed_cmd[1] != wheelspeed[0] or wheelspeed_cmd[0] != wheelspeed[1]:
                print(wheelspeed_cmd[1], wheelspeed[0], wheelspeed_cmd[0], wheelspeed[1])
                self._robotProxy.driveDirect(wheelspeed_cmd[0], wheelspeed_cmd[1]) #1.2.2.1.
            self.monitor_handle_socket(ready, conn, wheelspeed_limit) #1.2.3.4 
         
        status_history = [all_valid,valid_status,valid_persistence, last_heartbeat]
        print(status_history)
        return(status_history)   
            
    def wifi_signal_strength(self, sensor_data, wifi_status_history,ready,conn):
        wifi_strenth_threshold = 1 #1.1.3.3
        wifi_backup_limit = 10
        wifi_status = int(sensor_data[4])
        wifi_strength = int(sensor_data[5]) #1.1.3.1
        trigger_mode = not(wifi_status_history[0])
        backup_count = wifi_status_history[1]
        standby_mode = wifi_status_history[2]
        wheelspeed = self.read_Wheelspeed()
        self._conn = conn
        if wifi_status==1:
            if wifi_strength > wifi_strenth_threshold and trigger_mode != 1:
                print("wifi strength good")   #1.1.3.2 #1.1.3.3
                standby_mode = 0
            elif wifi_strength > wifi_strenth_threshold and trigger_mode == 1:
                wheelspeed = self.read_Wheelspeed() #1.1.3.2 #1.1.3.3
                if wheelspeed[2] != 0:
                    self._robotProxy.driveDirect(0, 0) #1.1.1.4 #1.1.3.6
                wheelspeed = self.read_Wheelspeed()
                print("L_Wheelspeed", wheelspeed[0])
                print("R_Wheelspeed", wheelspeed[1])
                print("O_Wheelspeed", wheelspeed[2])
                trigger_mode = 0 #1.1.3.6
            elif standby_mode == 1:
                print("Standby Mode")
            else:
                if trigger_mode == 1 and backup_count <= wifi_backup_limit:
                    #1.1.3.2 #1.1.3.5
                    if (ready[0]):
                        data = self._conn.recv(1024).strip()
                    print("Still backing up")
                    print("L_Wheelspeed", wheelspeed[0])
                    print("R_Wheelspeed", wheelspeed[1])
                    print("O_Wheelspeed", wheelspeed[2])
                    backup_count += 1
                elif trigger_mode == 1 and backup_count > 10:
                    #1.1.3.7
                    print('Backup limit reached')
                    standby_mode = 1
                    trigger_mode = 0
                    wheelspeed = self.read_Wheelspeed()
                    if self._robotProxy._robotCommand._isCreated == 1 and wheelspeed[2] != 0:
                        self._robotProxy.driveDirect(0, 0) #1.1.1.4
                        
                    
                else:
                    #1.1.3.2 #1.1.3.3
                    print("L_Wheelspeed", wheelspeed[0])
                    print("R_Wheelspeed", wheelspeed[1])
                    print("O_Wheelspeed", wheelspeed[2])
                    print("try to back up")
                    self._robotProxy.driveDirect(-wheelspeed[1], -wheelspeed[0])  #1.1.3.5
                    trigger_mode = 1
                    backup_count = 1
                    if (ready[0]):
                        data = self._conn.recv(1024).strip()
        else:
            print("wifi disconnected")
            if self._robotProxy._robotCommand._isCreated == 1:
                self._robotProxy._robotCommand._robot.toSafeMode()
        signal_strength_status = not(trigger_mode)
        wifi_status_history = [signal_strength_status,backup_count, standby_mode]
        return(wifi_status_history)
    