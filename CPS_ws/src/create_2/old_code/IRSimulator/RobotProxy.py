class RobotProxy:
    def __init__(self, robotState, robotCommand):
        self._robotState = robotState
        self._robotCommand = robotCommand
        self._subscriberList = []

    def connect(self):
        self._robotCommand.connect()

    def disconnect(self):
        self._robotCommand.disconnect()

    def startStream(self):
        self._robotCommand.startStream()

    def checkStream(self):
        self._robotCommand.checkStream()

    # whichLED bit field specifies which LEDS to turn on:
    # bit 2 = play, bit 3 = advance (green, full intensity)
    # whichLED = (0x1 << 1) (play on)
    # whichLED = (0x1 << 3) (advance on)
    # whichLED = (0x1 << 1) | (0x1 << 3) play and advance on
    # power LED color: range (0-255), 0 = green, 255 = red
    # power LED intensity: range (0-255), 0 = off, 255 = full intensity
    def setLEDs(self, whichLED, powerColor, powerIntensity):
        self._robotCommand.setLEDs(whichLED, powerColor, powerIntensity)

    # velocity in mm/sec, range -500 to 500 mm/s.
    # radius in mm, range -2000 to 2000 mm.
    def drive(self, velocity, radius):
        self._robotCommand.drive(velocity, radius)

    # velocity in mm/sec, range -500 to 500 mm/s.
    def driveDirect(self, rightWheelVelocity, leftWheelVelocity):
        self._robotCommand.driveDirect(rightWheelVelocity, leftWheelVelocity)

    # data:
    # -1 = abort - stops the current demo
    #  0 = cover - attempts to cover entire room
    #  1 = cover and dock - cover + use IR signal to dock 
    #      with home
    #  2 = spot cover - covers area around its starting
    #      position (spirals outward then inward)
    #  3 = mouse - searches for wall, follows wall
    #  4 = figure eight
    #  5 = wimp - drives forward when pushed from behind
    #  6 = home - drives toward virtual wall
    #  7 = tag = home + drives into multiple virtual walls
    #      (back and forth)
    #  8 = pachelbel (plays music)
    #  9 = banjo (plays note for each cliff sensor, use
    #      bumper to select chord)
    def demo(self, data):
        self._robotCommand.demo(data)

    def registerSubscriber(self, subscriber):
        pass

    def notify(self):
        pass

    def setSerialPortInfo(self, portString):
        self._robotCommand.setSerialPortInfo(portString)
