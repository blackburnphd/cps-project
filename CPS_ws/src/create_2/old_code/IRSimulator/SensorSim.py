'''
Created on Oct 12, 2015

@author: cjhoel
'''

class SensorSim:
    
    def __init__(self):
        '''
        Constructor
        '''
        
        
    def sensor_dataset(self, data_file = "V.T-2.1.csv"):
        #read data
        data_list=[]
        sens_input = open(data_file, "r")
            #read all the lines in the sensor input file and put the in the list called
            # all_inputs.  Then close the input file, we dont need it.
        all_inputs = sens_input.readlines()
        sens_input.close()
    
        for eachline in all_inputs:
            eachline = eachline.strip('\n')           #split the line by comma(',')
            break_line = eachline.split(',')
            if "Validity" in eachline:
                skip=1 #if its a header row don't append to the array
            else: 
                data_list.append(break_line)
        data_list.insert(0,len(data_list))
        print("loaded_data")
        return(data_list)
    
    