from enum import IntEnum
import create
import sys
import time
import StreamPacketFactory as spf
import RobotCommand as rc

class CommandSeq:
    def send(self, serial):
        print("send: ", bytearray(self.getCommand()))
        serial.write(bytearray(self.getCommand()))

class StartCommand(CommandSeq):
    def getCommand(self):
        return [ rc.OpCode.start ]

class PauseResumeCommand(CommandSeq):
    def __init__(self, resumeOn):
        self._resumeOn = resumeOn

    def getCommand(self):
        return [ rc.OpCode.pauseResumeStream, self._resumeOn ]

class StreamCommand(CommandSeq):
    def __init__(self):
        self._cmd = [ rc.OpCode.stream, 0 ]

    def addPacket(self, packetId):
        self._cmd[1] = self._cmd[1] + 1
        self._cmd.append(packetId)

    def getCommand(self):
        print("stream: ", self._cmd)
        return self._cmd

class SensorsCommand(CommandSeq):
    def __init__(self, packetId):
        self._packetId = packetId

    def getCommand(self):
        return [ rc.OpCode.sensors, self._packetId ]
        
class SimRobotCommand:

    def __init__(self, streamPacketFactory):
        self._isCreated = 0
        self._streamPacketFactory = streamPacketFactory

    def connect(self):
        self._robot = create.Create('sim')
        self._isCreated = 1

    def disconnect(self):
        self._robot.shutdown()
        self._isCreated = 0

    def send(self, commandSeq):
        return commandSeq.send(self._serial)

    def startStream(self):
        self._robot.start()

    def checkStream(self):
        # populate a robotState object, send to stream packet
        # factory and call its notify method.

        robotState = self._streamPacketFactory.getRobotState()
        robotState.reset()
        
        bumps = self._robot.getSensor("BUMPS_AND_WHEEL_DROPS")
        robotState._wheelDrop._caster = bumps[0]
        robotState._wheelDrop._left = bumps[1]
        robotState._wheelDrop._right = bumps[2]
        robotState._bump._left = bumps[3]
        robotState._bump._right = bumps[4]

        # simulation seems to return 1 for no cliff, but iRobot Create
        # open interface document specifies 0 for no cliff. That's
        # why I flip the bits here.
        robotState._cliff._left = int(not self._robot.getSensor("CLIFF_LEFT"))
        robotState._cliff._right = int(not self._robot.getSensor("CLIFF_RIGHT"))
        robotState._cliff._frontLeft = int(not self._robot.getSensor("CLIFF_FRONT_LEFT"))
        robotState._cliff._frontRight = int(not self._robot.getSensor("CLIFF_FRONT_RIGHT"))

        # create simulation doesn't support battery charge and capacity
        # robotState._battery._charge = self._robot.getSensor("BATTERY_CHARGE")
        # robotState._battery._capacity = self._robot.getSensor("BATTERY_CAPACITY")
        robotState._battery._charge = 0
        robotState._battery._capacity = 0
        
        # convert from cm to mm for velocities
        robotState._wheelVelocity._left = 10 * self._robot.getSensor("LEFT_VELOCITY")
        robotState._wheelVelocity._right = 10 * self._robot.getSensor("RIGHT_VELOCITY")
        robotState._wheelVelocity._overall = 10 * self._robot.getSensor("VELOCITY")
        
        self._streamPacketFactory.setRobotState(robotState)
        self._streamPacketFactory.notify()

    def drive(self, velocity, radius):
        self._robot.drive(velocity, radius)

    def driveDirect(self, rightWheelVelocity, leftWheelVelocity):
        # convert from mm to cm
        self._robot.driveDirect(leftWheelVelocity/10, rightWheelVelocity/10)

    # whichLED bit field specifies which LEDS to turn on:
    # bit 2 = play, bit 3 = advance (green, full intensity)
    # power LED color: range (0-255), 0 = green, 255 = red
    # power LED intensity: range (0-255), 0 = off, 255 = full intensity
    def setLEDs(self, whichLED, powerColor, powerIntensity):
        play = whichLED & (1 << 1)
        advance = whichLED & (1 << 3)
        self._robot.setLEDs(powerColor, powerIntensity, play, advance)

    def setSerialPortInfo(self, serialPortString):
        pass

    def demo(self, data):
        self._robot.demo(data)
