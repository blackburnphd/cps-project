import RobotProxy as rp
import RobotState as rs
import SimRobotCommand as rc
import StreamPacketFactory as spf

class SimRobotProxy(rp.RobotProxy):
    def __init__(self):
        self._robotState = rs.RobotState()
        self._streamPacketFactory = spf.StreamPacketFactory(self._robotState)
        self._robotCommand = rc.SimRobotCommand(self._streamPacketFactory)
        rp.RobotProxy.__init__(self, 
            self._robotState,
            self._robotCommand)

    def registerSubscriber(self, subscriber):
        self._streamPacketFactory.registerSubscriber(subscriber)

    def notify(self):
        for subscriber in self._subscriberList:
            subscriber.notify(self._robotState)

