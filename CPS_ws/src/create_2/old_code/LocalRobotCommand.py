#!/usr/bin/env python

from enum import IntEnum
import SerialComponent as ser
import StreamPacketFactory as spf
import time
import RobotCommand as rc
#import RobotCommand2 as rc
import rospy
from std_msgs.msg import String

class CommandSeq:
    def send(self, serial):
        print("send: ", bytearray(self.getCommand()))
        serial.write(bytearray(self.getCommand()))

class StartCommand(CommandSeq):
    def getCommand(self):
        return [ rc.OpCode.start ]

class PauseResumeCommand(CommandSeq):
    def __init__(self, resumeOn):
        self._resumeOn = resumeOn

    def getCommand(self):
        return [ rc.OpCode.pauseResumeStream, self._resumeOn ]

class StreamCommand(CommandSeq):
    def __init__(self):
        self._cmd = [ rc.OpCode.stream, 0 ]

    def addPacket(self, packetId):
        self._cmd[1] = self._cmd[1] + 1
        self._cmd.append(packetId)

    def getCommand(self):
        print("stream: ", self._cmd)
        return self._cmd

class SensorsCommand(CommandSeq):
    def __init__(self, packetId):
        self._packetId = packetId

    def getCommand(self):
        return [ rc.OpCode.sensors, self._packetId ]
        
class LocalRobotCommand:

    def __init__(self, streamPacketFactory):
        self._isSerialOpen = 0
        self._streamPacketFactory = streamPacketFactory
        self._serial = ser.SerialComponent()
        self._serialPortInfo = "/dev/ttyUSB0"
        pass

    def connect(self):
        self._serial.open(self._serialPortInfo)
        self._isSerialOpen = 1

    def disconnect(self):
        self._serial.close()
        self._isSerialOpen = 0

    def send(self, commandSeq):
        return commandSeq.send(self._serial)

    def startStream(self):
        self.send(StartCommand())
        time.sleep(0.5)
        strCmd = StreamCommand()
        strCmd.addPacket(spf.PacketId.group0)
        strCmd.addPacket(spf.PacketId.requestedVelocity)
        strCmd.addPacket(spf.PacketId.requestedRightVelocity)
        strCmd.addPacket(spf.PacketId.requestedLeftVelocity)
        strCmd.addPacket(spf.PacketId.batteryCapacity)
        strCmd.addPacket(spf.PacketId.batteryCharge)
        self.send(strCmd)

    def checkStream(self):
        count = 0
        readBytes = self._serial.tryRead(1)
        go = (len(readBytes) != 0)
        while (go):
            if (readBytes[0] == spf.PacketId.streamPacket):
                # number of bytes between n-bytes byte and checksum 
                numBytes = self._serial.blockRead(1)
                # adding one to get the trailing checksum
                packetBytes = self._serial.blockRead(int(numBytes[0]) + 1)
                # Assumes we already found the packet marker ('19')
                # and the N-bytes byte.
                # Send to streamPacketFactory to decode packets and
                # notify subscribers.
                self._streamPacketFactory.streamUpdate(packetBytes)
            readBytes = self._serial.tryRead(1)
            go = (len(readBytes) != 0)

    def genericDriveCmd(self, cmdOpCode, value1, value2):
        self._serial.write(bytearray([rc.OpCode.full]))
        time.sleep(0.1)

        value1_bytes = value1.to_bytes(2, byteorder='big', signed=True)
        value2_bytes = value2.to_bytes(2, byteorder='big', signed=True)
        command = [cmdOpCode,
            value1_bytes[0], value1_bytes[1],
            value2_bytes[0], value2_bytes[1] ]
        byteStr = bytearray(command)
        print("command = ", command, byteStr)
        self._serial.write(byteStr)

    def drive(self, velocity, radius):
        self.genericDriveCmd(rc.OpCode.drive, velocity, radius)

    def driveDirect(self, rightWheelVelocity, leftWheelVelocity):
        self.genericDriveCmd(rc.OpCode.driveDirect, rightWheelVelocity, 
            leftWheelVelocity)

    # whichLED bit field specifies which LEDS to turn on:
    # bit 1 = play, bit 3 = advance (green, full intensity)
    # power LED color: range (0-255), 0 = green, 255 = red
    # power LED intensity: range (0-255), 0 = off, 255 = full intensity
    def setLEDs(self, whichLED, powerColor, powerIntensity):
        command = [rc.OpCode.full, 
            rc.OpCode.leds, whichLED, powerColor, powerIntensity]
        print(command)
        byteStr = bytearray(command)
        self._serial.write(byteStr)

    def setSerialPortInfo(self, serialPortString):
        self._serialPortInfo = serialPortString

    def demo(self):
        #if (data == -1):
         #   data = 255
        command = [rc.OpCode.demo]
        print(command)
        byteStr = bytearray(command)
        self._serial.write(byteStr)

    def cover(self):
        command = [rc.OpCode.cover]
        print(command)
        byteStr = bytearray(command)
        self._serial.write(byteStr)

    def coverAndDock(self):
        command = [rc.OpCode.coverAndDock]
        print(command)
        byteStr = bytearray(command)
        self._serial.write(byteStr)

    def spot(self):
        command = [rc.OpCode.spot]
        print(command)
        byteStr = bytearray(command)
        self._serial.write(byteStr)

    def start(self):
        command = [rc.OpCode.start]
        print(command)
        byteStr = bytearray(command)
        self._serial.write(byteStr)
