#!/usr/bin/env python

import socket
import select
import LocalRobotProxy as lrp
import SimRobotProxy as srp
import create
import time
import sys
import rospy
from std_msgs.msg import String

#sys.argv.append("-sim")
      
class SocketServer:

    def __init__(self, addr, timeout):
        self._host, self._port = addr
        self._timeout = timeout

        self._s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        print("server bind to ", addr)
        self._s.bind(addr)
        self._s.listen(5)

    def serve_forever(self):
        time.sleep(5)
        while 1:
            print("server accept")
            self._conn, addr = self._s.accept()
            self._conn.setblocking(0)
            found_end = False
            while (not found_end):
                try:
                    ready = select.select([self._conn],[],[],0.5)
                    if (ready[0]):
                        print("server select ready")
                        found_end = self.handle_socket(self._conn)
                        if (found_end):
                            print("server close")
                            self._conn.close()
                    else:
                        self.handle_timeout()

                except socket.timeout:
                # timeout occurred
                    pass

class NetworkServer(SocketServer):
    def __init__(self):
        TIMEOUT = 0.5
        # MRB - changed this to 127.0.0.1 6/26/2015
        #HOST = '127.0.0.1' # allows connection requests from any address 
        HOST = '' # allows connection requests from any address 
        PORT = 9999
        SocketServer.__init__(self, (HOST, PORT), TIMEOUT)
        
        if len(sys.argv) > 1:
           if sys.argv[1] == '-sim':
              self._robotProxy = srp.SimRobotProxy()
        else:
            self._robotProxy = lrp.LocalRobotProxy()
        self._robotProxy.registerSubscriber(self)
        self._lastCmd = ""
        #hname = socket.gethostname()
        self._serialPortInfo = "/dev/ttyUSB0"
        

    def handle_socket(self, conn):
        self._conn = conn
        data = self._conn.recv(1024).strip()
        if not data:
            print('connection lost')
            return True
        else:
            # convert bytes to string
            dataString = data.decode("utf-8")
            print("received: ", dataString)

            # convert string to list of tokens
            tokens = dataString.split()

            # if command recognized, send command to proxy
            if (tokens[0] == "startStream"):
                serialPortInfo = tokens[1]
                self._robotProxy.setSerialPortInfo(serialPortInfo)
                self._robotProxy.connect()
                self._robotProxy.startStream()
            elif (tokens[0] == "led"):
                whichLED = int(tokens[1])
                powerColor = int(tokens[2])
                powerIntensity = int(tokens[3])
                self._robotProxy.setLEDs(whichLED, powerColor, powerIntensity)
            elif (tokens[0] == "drive"):
                p1 = int(tokens[1])
                p2 = int(tokens[2])
                self._robotProxy.drive(p1, p2)
            elif (tokens[0] == "driveDirect"):
                p1 = int(tokens[1])
                p2 = int(tokens[2])
                self._robotProxy.driveDirect(p1, p2)
            elif (tokens[0] == "demo"):
                self._robotProxy.demo()
            elif (tokens[0] == "start"):
                self._robotProxy.start()
            elif (tokens[0] == "cover"):
                self._robotProxy.cover()
            elif (tokens[0] == "coverAndDock"):
                self._robotProxy.coverAndDock()
            elif (tokens[0] == "spot"):
                self._robotProxy.spot()
            return False
       
    def handle_timeout(self):
        # get state from stream
        self._robotProxy.checkStream()

    def notify(self, robotState):
        strOut = "stream"
        strOut += " "+str(int(robotState._wheelDrop._caster))
        strOut += " "+str(int(robotState._wheelDrop._left))
        strOut += " "+str(int(robotState._wheelDrop._right))
        strOut += " "+str(int(robotState._bump._left))
        strOut += " "+str(int(robotState._bump._right))
        strOut += " "+str(int(robotState._cliff._left))
        strOut += " "+str(int(robotState._cliff._right))
        strOut += " "+str(int(robotState._cliff._frontLeft))
        strOut += " "+str(int(robotState._cliff._frontRight))
        strOut += " "+str(int(robotState._battery._charge))
        strOut += " "+str(int(robotState._battery._capacity))
        strOut += " "+str(int(robotState._wheelVelocity._left))
        strOut += " "+str(int(robotState._wheelVelocity._right))
        strOut += " "+str(int(robotState._wheelVelocity._overall))
        strOut += " $"
        #print(strOut)
        if (strOut != self._lastCmd):
            outBytes = strOut.encode('utf-8')
            self._conn.sendall(outBytes)
            self._lastCmd = strOut


s = NetworkServer()
s.serve_forever()
