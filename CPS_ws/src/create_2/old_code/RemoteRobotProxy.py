#!/usr/bin/env python

import RobotProxy as rp
import RobotState as rs
import RemoteRobotCommand as rc
import rospy
from std_msgs.msg import String

class RemoteRobotProxy(rp.RobotProxy):
    def __init__(self):
        self._robotState = rs.RobotState()
        self._robotCommand = rc.RemoteRobotCommand(self._robotState)
        rp.RobotProxy.__init__(self, 
            self._robotState,
            self._robotCommand)

    def registerSubscriber(self, subscriber):
        self._robotCommand.registerSubscriber(subscriber)

    def notify(self):
        self._robotCommand.notify(self._robotState)

    def setAddr(self, addrVar, portVar):
        self._robotCommand.setAddr(addrVar, portVar)
