#!/usr/bin/env python

# tested with PySerial 2.7
import serial
import rospy
from std_msgs.msg import String

class SerialComponent:

    def __init__(self):
        self._isOpen = 0

    def open(self, portString):
        if (self._isOpen == 1):
            print("SerialComponent: closing before reopening")
            self.close()

        #self._handle = serial.Serial(port="/dev/ttyUSB0",
        #self._handle = serial.Serial(port="/dev/ttyS0",
        self._handle = serial.Serial(port=portString,
            baudrate=115200 ,
            bytesize=8,
            stopbits=serial.STOPBITS_ONE,
            xonxoff=False,
            timeout=0) # seconds
        self._isOpen = 1

    def close(self):
        if (self._isOpen):
            self._handle.close()
        self._isOpen = 0
       
    def write(self, byteSequence):
        if (self._isOpen == 0):
            self.open()
        wbytes = self._handle.write(bytes(byteSequence))
        print("write ", wbytes, " number of bytes")
        blist = list(byteSequence)

    def tryRead(self, numBytes=1024):
        return self._handle.read(numBytes)

    def blockRead(self, numBytes=1024):
        messageList = []
        val = self.tryRead(numBytes)
        messageList.append(val)
        totalRead = len(val)
        while (totalRead < numBytes):
            moreBytes = self.tryRead(numBytes-totalRead)
            readCount = len(moreBytes)
            if (readCount > 0):
                messageList.append(moreBytes)
                totalRead += readCount
        return b"".join(messageList)

    def read(self, numBytes=1024):
        if (self._isOpen == 0):
            self.open()
        val = self._handle.read(numBytes)
        if (len(val) == 0):
            print("serial read returned nil")
        #else:
        #    print("serial read returned %d bytes (%d ...)" % (len(val), int(val[0])))
        
        return val

