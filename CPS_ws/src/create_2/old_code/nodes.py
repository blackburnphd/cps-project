#!/usr/bin/env python
import rospy
from std_msgs.msg import String
from gazebo_msgs.msg import ModelState
from gazebo_msgs.msg import ModelStates
from gazebo_msgs.msg import LinkState
from gazebo_msgs.srv import *
from geometry_msgs.msg import *

def spawnCreate():
    #Spawn a robot model in the center of the world
    rospy.wait_for_service('gazebo/spawn_sdf_model')
    spawn = rospy.ServiceProxy("gazebo/spawn_sdf_model", SpawnModel)
    try:
        model_name = "create"
        model_path = '/home/dan/CPS_ws/src/create_description/sdf/model-1_4.sdf'
        sdf = open(model_path, 'r')
        model_xml = sdf.read()
        robot_namespace = "create"
        initial_position = Pose()
        initial_position.position.x = 0
        initial_position.position.y = 0
        initial_position.position.z = 0
        initial_position.orientation.x = 0
        initial_position.orientation.y = 0
        initial_position.orientation.z = 0
        initial_position.orientation.w = 0
        reference_frame = "world"
        response = spawn(model_name, 
            model_xml, 
            robot_namespace,
            initial_position,
            reference_frame)
    except rospy.ServiceException as e:
        print("Spawn Create failed: %s"%e)
        return False

def driveWheelsNode(rightWheel, leftWheel):
    rpub = rospy.Publisher('gazebo/set_link_state', LinkState, queue_size=10)
    msg = LinkState()
    msg.link_name = "right_wheel"
    # msg.twist.linear.z = rightWheel
    msg.twist.angular.y = rightWheel
    msg.reference_frame = 'world'
    rospy.loginfo(msg.link_name)
    rospy.loginfo(msg.twist.angular)
    rpub.publish(msg)
    lpub = rospy.Publisher('gazebo/set_link_state', LinkState, queue_size=10)
    msg = LinkState()
    msg.link_name = "left_wheel"
    # msg.twist.linear.z = leftWheel
    msg.twist.angular.y = leftWheel
    msg.reference_frame = 'world'
    rospy.loginfo(msg.link_name)
    rospy.loginfo(msg.twist.angular)
    lpub.publish(msg)

def driveRobotNode(velocity, angle):
    #Set up publisher to send a message to Gazebo
    model_name = "create"
    pub = rospy.Publisher('gazebo/set_model_state', ModelState, queue_size=10)
    posArray = getRobotPosition(model_name)
    rospy.loginfo(posArray)
    if not posArray:
        return
    #Set the message data with the current position and orientation of the robot 
    #and a user-input velocity and angle
    msg = ModelState()
    msg.model_name = posArray[0]
    msg.pose.position.x = posArray[1][0]
    msg.pose.position.y = posArray[1][1]
    msg.pose.position.z = posArray[1][2]
    msg.pose.orientation.x = posArray[2][0]
    msg.pose.orientation.y = posArray[2][1]
    msg.pose.orientation.z = posArray[2][2]
    msg.pose.orientation.w = posArray[2][3]
    msg.twist.linear.x = velocity
    msg.twist.angular.z = angle
    msg.reference_frame = 'world'
    rospy.loginfo(msg)
    pub.publish(msg)

def teleopDriveRobot():
    #Not sure what data type this is
    model_name = "create"
    sub = rospy.Subscriber("cmd_vel", "teleop_twist_keyboard/cmd_vel")
    pub = rospy.Publisher('gazebo/set_model_state', ModelState, queue_size=10)
    posArray = getRobotPosition(model_name)
    rospy.loginfo(posArray)
    if not posArray:
        return
    #Set the message data with the current position and orientation of the robot 
    #and a user-input velocity and angle
    msg = ModelState()
    msg.model_name = posArray[0]
    msg.pose.position.x = posArray[1][0]
    msg.pose.position.y = posArray[1][1]
    msg.pose.position.z = posArray[1][2]
    msg.pose.orientation.x = posArray[2][0]
    msg.pose.orientation.y = posArray[2][1]
    msg.pose.orientation.z = posArray[2][2]
    msg.pose.orientation.w = posArray[2][3]
    msg.twist.linear.x = sub.linear.x
    msg.twist.angular.z = sub.angular.z
    msg.reference_frame = 'world'
    rospy.loginfo(msg)
    pub.publish(msg)

# def driveRobotService(velocity, angle):
#     rospy.wait_for_service('gazebo/set_model_state')
#     model_state = rospy.ServiceProxy("gazebo/set_model_state", SetModelState)
#     try:
#         model_name = "create"
#         posArray = getRobotPosition(model_name)
#         rospy.loginfo(posArray)
#         if not posArray:
#             return
#         #Set the message data with the current position and orientation of the robot 
#         #and a user-input velocity and angle
#         state = ModelState()
#         state.model_name = posArray[0]
#         state.pose.position.x = posArray[1][0]
#         state.pose.position.y = posArray[1][1]
#         state.pose.position.z = posArray[1][2]
#         state.pose.orientation.x = posArray[2][0]
#         state.pose.orientation.y = posArray[2][1]
#         state.pose.orientation.z = posArray[2][2]
#         state.pose.orientation.w = posArray[2][3]
#         state.twist.linear.x = velocity
#         state.twist.angular.z = angle
#         state.reference_frame = 'world'
#         rospy.loginfo(state)
#         SetModelState()
#     except rospy.ServiceException as e:
#         print("Set model state failed: %s"%e)
#         return False

def getRobotPosition(model_name):
    #Initiate an array that holds the state of the robot
    nameAr = model_name
    posAr = [0, 0, 0]
    oriAr = [0, 0, 0, 0]
    linAr = [0, 0, 0]
    angAr = [0, 0, 0]
    states = [nameAr, posAr, oriAr, linAr, angAr]
    #Wait for the GetModelState service
    rospy.wait_for_service('/gazebo/get_model_state')
    gms = rospy.ServiceProxy('/gazebo/get_model_state', GetModelState)
    try:
        #Call the service function and set the states array with the state of the robot
        response = gms(model_name, "world")
        states[0] = model_name
        states[1][0] = response.pose.position.x
        states[1][1] = response.pose.position.y
        states[1][2] = response.pose.position.z
        states[2][0] = response.pose.orientation.x
        states[2][1] = response.pose.orientation.y
        states[2][2] = response.pose.orientation.z
        states[2][3] = response.pose.orientation.w
        states[3][0] = response.twist.linear.x
        states[3][1] = response.twist.linear.y
        states[3][2] = response.twist.linear.z
        states[4][0] = response.twist.angular.x
        states[4][1] = response.twist.angular.y
        states[4][2] = response.twist.angular.z
        return states
    except rospy.ServiceException, e:
        print "Get robot position failed: %s"%e

def reset(model_name):
    #Reset the robot's position, orientation, velocity, and rotation to 0
    pub = rospy.Publisher('gazebo/set_model_state', ModelState, queue_size=10)
    msg = ModelState()
    msg.model_name = model_name
    msg.pose.position.x = 0
    msg.pose.position.y = 0
    msg.pose.position.z = 0
    msg.pose.orientation.x = 0
    msg.pose.orientation.y = 0
    msg.pose.orientation.z = 0
    msg.pose.orientation.w = 0
    msg.twist.linear.x = 0
    msg.twist.linear.y = 0
    msg.twist.linear.z = 0
    msg.twist.angular.x = 0
    msg.twist.angular.y = 0
    msg.twist.angular.z = 0
    msg.reference_frame = 'world'
    rospy.loginfo(msg)
    pub.publish(msg)