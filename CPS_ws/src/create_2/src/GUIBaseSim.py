#!/usr/bin/env python

import Tkinter as tk
import rospy

class StatusLabel(tk.Label):
    def __init__(self, master, guiClient):
        tk.Label.__init__(self, master, text="")
        #guiClient.registerSubscriber(self)
        self.buildString(guiClient.getRobotState())

    def notify(self, robotState):
        self.buildString(robotState)

class WheelVelocity(StatusLabel):
    def __init__(self, master, guiClient):
        StatusLabel.__init__(self, master, guiClient)

    def buildString(self, robotState):
        #self["text"] = "Wheel Velocity: L %d, R %d" % (
            #robotState._wheelVelocity._left, 
            #robotState._wheelVelocity._right)
        print("Wheel Velocity")

class WheelDrop(StatusLabel):
    def __init__(self, master, guiClient):
        self._timer = 0
        StatusLabel.__init__(self, master, guiClient)

    def buildString(self, robotState):
        self._timer += 1
        #self["text"] = "Wheel Drop: C %d, L %d, R %d" % (
            #robotState._wheelDrop._caster, 
            #robotState._wheelDrop._left,
            #robotState._wheelDrop._right)
        print("Wheel Drop")

class Cliff(StatusLabel):
    def __init__(self, master, guiClient):
        StatusLabel.__init__(self, master, guiClient)

    def buildString(self, robotState):
        #self["text"] = "Cliff: FL %d, FR %d, L %d, R %d" % (
            #robotState._cliff._frontLeft, 
            #robotState._cliff._frontRight,
            #robotState._cliff._left,
            #robotState._cliff._right)
        print("Cliff")

class CommandDemo(tk.Frame):
    def __init__(self, master, guiClient):
        tk.Frame.__init__(self, master, borderwidth=2, relief="groove")

        self._topFrame = tk.Frame(self)
        self._topFrame.pack(side="top")

        self._bottomFrame = tk.Frame(self)
        self._bottomFrame.pack(side="bottom")

        self._guiClient = guiClient

        self._demoBtn = tk.Button(self._topFrame,
            text="demo(option)",
            command=self.buttonCallback)
        self._demoBtn.pack(side="left")

        optionList = ("abort", "max clean", "clean", 
            "cover and dock", "spot cover")
        self._option = tk.StringVar()
        self._option.set(optionList[0])
        self._optionMenu = tk.OptionMenu(self._topFrame, self._option,
            *optionList)
        self._optionMenu.pack(side="right")

    def buttonCallback(self):  
        self._guiClient.demo(self._option.get())

class CommandDrive(tk.Frame):
    def __init__(self, master, guiClient):
        tk.Frame.__init__(self, master, borderwidth=2, relief="groove")

        self._topFrame = tk.Frame(self)
        self._topFrame.pack(side="top")

        self._bottomFrame = tk.Frame(self)
        self._bottomFrame.pack(side="bottom")

        self._guiClient = guiClient

        self._driveBtn = tk.Button(self._topFrame,
            text="drive(velocity, radius)",
            command=self.buttonCallback)
        self._driveBtn.pack(side="left")

        self._velocityVar = tk.StringVar()
        self._velocityVar.set("0")
        self._velocityEntry = tk.Entry(self._bottomFrame, 
            textvariable=self._velocityVar)
        self._velocityEntry.pack(side="left")

        self._radiusVar = tk.StringVar()
        self._radiusVar.set("0")
        self._radiusEntry = tk.Entry(self._bottomFrame, 
            textvariable=self._radiusVar)
        self._radiusEntry.pack(side="left")

    def buttonCallback(self):
        velocity = int(self._velocityVar.get())
        radius = int(self._radiusVar.get())
        self._guiClient.drive(velocity, radius)

class CommandDriveDirect(tk.Frame):
    def __init__(self, master, guiClient):
        tk.Frame.__init__(self, master, borderwidth=2, relief="groove")

        self._topFrame = tk.Frame(self)
        self._topFrame.pack(side="top")

        self._bottomFrame = tk.Frame(self)
        self._bottomFrame.pack(side="bottom")

        self._guiClient = guiClient

        self._driveBtn = tk.Button(self._topFrame,
            text="driveDirect(left velocity, right velocity)",
            command=self.buttonCallback)
        self._driveBtn.pack(side="left")

        self._leftVelocityVar = tk.StringVar()
        self._leftVelocityVar.set("0")
        self._leftVelocityEntry = tk.Entry(self._bottomFrame, 
            textvariable=self._leftVelocityVar)
        self._leftVelocityEntry.pack(side="left")

        self._rightVelocityVar = tk.StringVar()
        self._rightVelocityVar.set("0")
        self._rightVelocityEntry = tk.Entry(self._bottomFrame, 
            textvariable=self._rightVelocityVar)
        self._rightVelocityEntry.pack(side="right")

    def buttonCallback(self):
        rightVelocity = int(self._rightVelocityVar.get())
        leftVelocity = int(self._leftVelocityVar.get())
        self._guiClient.driveDirect(rightVelocity, leftVelocity)

class CommandDriveToLocation(tk.Frame):
    def __init__(self, master, guiClient):
        tk.Frame.__init__(self, master, borderwidth=2, relief="groove")

        self._topFrame = tk.Frame(self)
        self._topFrame.pack(side="top")

        self._bottomFrame = tk.Frame(self)
        self._bottomFrame.pack(side="bottom")

        self._guiClient = guiClient

        self._driveBtn = tk.Button(self._topFrame,
            text="Drive to (x, y)",
            command=self.buttonCallback)
        self._driveBtn.pack(side="left")

        self._xVar = tk.StringVar()
        self._xVar.set("0")
        self._xEntry = tk.Entry(self._bottomFrame, 
            textvariable=self._xVar)
        self._xEntry.pack(side="left")

        self._yVar = tk.StringVar()
        self._yVar.set("0")
        self._yEntry = tk.Entry(self._bottomFrame, 
            textvariable=self._yVar)
        self._yEntry.pack(side="left")

    def buttonCallback(self):
        x = int(self._xVar.get())
        y = int(self._yVar.get())
        self._guiClient.driveToLocation(x, y)

class CommandStopMoving(tk.Button):
    def __init__(self, master, guiClient):
        tk.Button.__init__(self, master,
            text="Stop Moving",
            command=self.buttonCallback)
        self._guiClient = guiClient

    def buttonCallback(self):
        self._guiClient.drive(0, 0)

class printModelInfo(tk.Frame):
    def __init__(self, master, guiClient):
        tk.Frame.__init__(self, master, borderwidth=2, relief="groove")

        self._topFrame = tk.Frame(self)
        self._topFrame.pack(side="top")

        self._bottomFrame = tk.Frame(self)
        self._bottomFrame.pack(side="bottom")

        self._guiClient = guiClient

        self._robotState = tk.StringVar()
        self._robotState.set("0")
        self._robotStateEntry = tk.Entry(self._bottomFrame, 
            textvariable=self._robotState)
        self._robotStateEntry.pack(side="bottom")

class ClientApp(tk.Frame):
    def __init__(self, guiClient, master=None):
        tk.Frame.__init__(self, master)
        self.pack()

        self._guiClient = guiClient

        self._resetModel = tk.Button(self, text="Reset Model", command = self.resetModel)
        self._resetModel.pack(side="bottom", fill='x')

        self._stop = CommandStopMoving(self, self._guiClient)
        self._stop.pack(side="bottom", fill='x')

        self._powerFrame = tk.Frame(self)
        self._powerFrame.pack(side="bottom", fill='x')

        # Button to drive the robot to a given location (incomplete)
        # self._commandDrive = CommandDriveToLocation(self, self._guiClient)
        # self._commandDrive.pack(side="bottom")

        self._commandDrive = CommandDrive(self, self._guiClient)
        self._commandDrive.pack(side="bottom")

        self._commandDrive = CommandDriveDirect(self, self._guiClient)
        self._commandDrive.pack(side="bottom")

    # Reset Gazebo model
    def resetModel(self):
        self._guiClient.resetModel("create")

    def updateGUI(self):
        return

def updateCallback(robotState):
    app.stateChange(robotState)
