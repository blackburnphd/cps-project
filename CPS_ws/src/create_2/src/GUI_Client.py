#!/usr/bin/env python
import rospy
import nodes
from std_msgs.msg import String
from create_2.srv import *
from create_2.msg import IntList
from threading import Thread
import RobotState as rs

class GUI_Client:
    def __init__(self, node_name):
        rospy.init_node(node_name)
        self._robotState = rs.RobotState()
        self.openThreads = True
        rospy.loginfo("GUI Client is ready")
        self.subscribeThread = Thread(target = self.subscribe)
        self.subscribeThread.start()

    # Subscribe to and process topics
    def subscribe(self):
        rospy.Subscriber('robotState', IntList, self.getRobotStateTopic)
        while self.openThreads:
            x = 1
        #rospy.spin()

    # Connect to robot
    def connect(self, serialPortVar):
        #Code for service request
        try:
            # Call connect service
            connect = rospy.ServiceProxy('connect', Connect)
            respl = connect(serialPortVar)
            rospy.loginfo("Successfully queried connection service to port: %s"%(serialPortVar))
            return True
        except rospy.ServiceException as e:
            rospy.logerr("Connection service query to port %s has failed: %s"%(serialPortVar, e))
            return False

    # Get robotState message
    def getRobotStateTopic(self, msg):
        dataName = msg.name
        rospy.logdebug("Robot State msg.name: %s" %(dataName))
        data = msg.data
        rospy.logdebug("Robot State msg.data: %s" %("".join(str(e) for e in data)))
        if dataName == "Battery":
            self._robotState._battery._capacity = data[0]
            self._robotState._battery._charge = data[1]
            self._robotState._battery._chargingState = data[2]
        elif dataName == "Mode":
            self._robotState._mode._oiMode = data[0]

    def setLEDs(self, whichLED, color, intensity):
        #robotproxy.setleds
        try:
            # Call setLEDs service
            setLEDs = rospy.ServiceProxy('setLEDs', SetLEDs)
            respl = setLEDs(whichLED, color, intensity)
            rospy.loginfo("Successfully queried setLEDs service: (%s,%s,%s)"%(whichLED, color, intensity))
        except rospy.ServiceException as e:
            rospy.logerr("setLEDs service (%s,%s,%s) query has failed: %s"%(whichLED, color, intensity, e))

    def drive(self, velocity, radius):
        # rospy.wait_for_service('drive')
        try:
            # Call drive service
            nodes.driveRobotNode(velocity, radius)
            # drive = rospy.ServiceProxy('drive', Drive)
            # respl = drive(velocity, radius)
            # rospy.loginfo("Successfully queried drive service: (%s,%s)"%(velocity, radius))
        except rospy.ServiceException as e:
            rospy.logerr("drive service (%s,%s) query has failed: %s"%(velocity, radius, e))

    def demo(self, demoType):
        rospy.wait_for_service('demo')
        try:
            # Call demo service
            demo = rospy.ServiceProxy('demo', Demo)
            respl = demo(demoType)
            rospy.loginfo("Successfully queried demo service: %s"%(demoType))
        except rospy.ServiceException as e:
            rospy.logerr("demo service %s query has failed: %s"%(demoType, e))
        #max clean, clean, cover and dock, spot, start

    def driveDirect(self, rightVelocity, leftVelocity):
        #robotproxy.drivedirect
        rospy.wait_for_service('driveDirect')
        try:
            # Call driveDirect service
            driveDirect = rospy.ServiceProxy('driveDirect', DriveDirect)
            driveDirect(rightVelocity, leftVelocity)
            rospy.loginfo("Successfully queried driveDirect service: (%s, %s)"%(rightVelocity, leftVelocity))
        except rospy.ServiceException as e:
            rospy.logerr("driveDirect service query has failed: %s"%(e))

    def testSend(self):
        rospy.wait_for_service('testService')
        try:
            # Call test service
            print("making service proxy")
            test = rospy.ServiceProxy('testService', Test)
            print("invoking test(69)")
            respl = test(69)
            print("return result%s"%(respl.resp))
            return respl.resp
        except rospy.ServiceException as e:
            print("Test service call failed: %s"%e)
            return False

    # Set robot mode
    def setMode(self, mode):
        rospy.wait_for_service('setMode')
        try:
            # Call mode service
            modeService = rospy.ServiceProxy('setMode', SetMode)
            respl = modeService(mode)
            rospy.loginfo("Successfully queried mode service: %s Current mode is %s"%(mode, respl.actualMode))
            msg = IntList()
            msg.name = "Mode"
            msg.data = [respl.actualMode]
            self.getRobotStateTopic(msg)
        except rospy.ServiceException as e:
            rospy.logerr("mode service %s query has failed: %s"%(mode, e))

    # Send power command
    def powerCommand(self, command):
        rospy.wait_for_service('powerCommand')
        try:
            # Call startCommand service
            start = rospy.ServiceProxy('powerCommand', PowerCommand)
            start(command)
            rospy.loginfo("Successfully queried powerCommand service: %s"%command)
        except rospy.ServiceException as e:
            rospy.logerr("powerCommand service %s query has failed: %s"%(command, e))

    def resetModel(self):
        nodes.reset("create")
        return

    def getRobotState(self):
        return self._robotState

    def close(self):
        self.openThreads = False
        self.subscribeThread.join()
