#!/usr/bin/env python
import rospy
import simRobotFunctions
from std_msgs.msg import String
from create_2.srv import *
from create_2.msg import IntList
from gazebo_msgs.msg import ModelState
from gazebo_msgs.msg import ModelStates

class GUI_Client:
    def __init__(self, node_name):
        rospy.init_node(node_name)
        self._robotState = ""
        rospy.Subscriber('robotState', IntList, self.getRobotState)
        rospy.loginfo("GUI Client is ready")   

    #Return the position and movement information of the model
    def getRobotState(self):
        robotPosition = simRobotFunctions.getRobotPosition("create")
        return robotPosition
        
    #Call function to drive the robot to a given location
    def driveToLocation(self, desiredX, desiredY):
        simRobotFunctions.driveToLocation(desiredX, desiredY)

    #Call function to drive the robot given a velocity and radius
    def drive(self, velocity, radius):
        simRobotFunctions.driveRobot(velocity, radius)

    #Call function to drive each wheel individually
    def driveDirect(self, rightVelocity, leftVelocity):
        simRobotFunctions.driveWheels(rightVelocity, leftVelocity)
        
    #Reset the model to a stationary state in the center of the world 
    def resetModel(self, modelName):
        simRobotFunctions.reset(modelName)

    def getRobotState(self):
        return self._robotState

    def close(self):
        self.openThreads = False
        self.subscribeThread.join()