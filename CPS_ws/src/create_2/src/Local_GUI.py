#!/usr/bin/env python
import Tkinter as tk
import GUIBase as gb
import GUI_Client as gc

if __name__ == "__main__":
    guiClient = gc.GUI_Client("gui_client")
    root = tk.Tk()
    app = gb.ClientApp(guiClient, master=root)

    #guiClient.startSubscribing()
    root.mainloop()
