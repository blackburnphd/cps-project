#!/usr/bin/env python
import Tkinter as tk
import GUIBaseSim as gbs
import GUI_Client_Sim as gcs

if __name__ == "__main__":
    guiClient = gcs.GUI_Client("gui_client")
    root = tk.Tk()
    app = gbs.ClientApp(guiClient, master=root)
    root.mainloop()
