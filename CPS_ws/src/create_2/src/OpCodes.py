

class OpCode():
    # getting started
    start = 128     # starts the OI. must always be the first command sent. also switches mode to passive
    reset = 7       # resets robot as if battery was replaced
    stop = 173      # stops streams and robot ceases to respond
    baud = 129      # immediately preceeds baud code (0-11)
    # modes
    safe = 131      # enables user command, but automatically switches to passive when certain conditions are met
    full = 132      # complete user control
    # clean commands
    clean = 135     # default cleaning cycle    
    max_clean = 136 # clean until battery is dead
    spot = 134      # spot cleaning mode
    dock = 143      # seek dock
    power = 133     # powers down robot
    # actuator commands
    drive = 137
    driveDirect = 145
    drivePWM = 146  # control each wheel independently
    lowSideDrivers = 138    # motion of brushes
    pwmLowSideDrivers = 144 # motion of brushes
    leds = 139
    digitalOutputs = 147
    sendIr = 151
    song = 140
    playSong = 141
    # input commands
    sensors = 142
    queryList = 149
    stream = 148
    pauseResumeStream = 150
    # script commands
    script = 152
    playScript = 153
    showScript = 154
    # wait commands
    waitTime = 155
    waitDistance = 156
    waitAngle = 157
    waitEvent = 158
