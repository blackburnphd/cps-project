from enum import IntEnum
import struct

def isBitSet(dataByte, bitIndex):
    return int(dataByte & (1<<bitIndex)) != 0

class PacketId(IntEnum):
    streamPacket = 19
    group0 = 0
    group1 = 1
    group2 = 2
    group3 = 3
    group4 = 4
    group5 = 5
    group6 = 6
    bumpsAndWheelDrops = 7
    wall = 8
    cliffLeft = 9
    cliffFrontLeft = 10
    cliffFrontRight = 11
    cliffRight = 12
    virtualWall = 13
    lowSideDriver = 14
    infrared = 17
    buttons = 18
    distance = 19
    angle = 20
    chargingState = 21
    voltage = 22
    current = 23
    batteryTemperature = 24
    batteryCharge = 25
    batteryCapacity = 26
    wallSignal = 27
    cliffLeftSignal = 28
    cliffFrontLeftSignal = 29
    cliffFrontRightSignal = 30
    cliffRightSignal = 31
    cargoBayDigitalInputs = 32
    cargoBayAnalogSignal = 33
    chargingSourceAvailable = 34
    oiMode = 35
    songNumber = 36
    songPlaying = 37
    numberOfStreamPackets = 38
    requestedVelocity = 39
    requestedRadius = 40
    requestedRightVelocity = 41
    requestedLeftVelocity = 42

class BumpAndDropPacket:
    def __init__(self):
        # self._wheelDropCaster = 0 this one is not in Create 2
        self._wheelDropLeft = 0
        self._wheelDropRight = 0
        self._wheelBumpLeft = 0
        self._wheelBumpRight = 0

    def getPacketSize(self):
        return 2

    # first byte is packet id
    def populate(self, dataBytes):
        data = dataBytes[1]
        # self._wheelDropCaster = isBitSet(data, 4) not in Create 2
        self._wheelDropLeft = isBitSet(data, 3)
        self._wheelDropRight = isBitSet(data, 2)
        self._wheelBumpLeft = isBitSet(data, 1)
        self._wheelBumpRight = isBitSet(data, 0)

class Group0Packet:
    def __init__(self):
        self._wheelDropCaster = 0
        self._wheelDropLeft = 0
        self._wheelDropRight = 0
        self._wheelBumpLeft = 0
        self._wheelBumpRight = 0
        self._cliffLeft = 0
        self._cliffRight = 0
        self._cliffFrontLeft = 0
        self._cliffFrontRight = 0

    def getPacketSize(self):
        return 27

    # first byte is packet id
    def populate(self, dataBytes):
        byte1 = dataBytes[1]
        self._wheelDropCaster = isBitSet(byte1, 4)
        self._wheelDropLeft = isBitSet(byte1, 3)
        self._wheelDropRight = isBitSet(byte1, 2)
        self._wheelBumpLeft = isBitSet(byte1, 1)
        self._wheelBumpRight = isBitSet(byte1, 0)

        self._cliffLeft = int(dataBytes[3])
        self._cliffFrontLeft = int(dataBytes[4])
        self._cliffFrontRight = int(dataBytes[5])
        self._cliffRight = int(dataBytes[6])

# used for both capacity and charge
class BatteryPacket:
    def __init__(self):
        self._value = 0.0

    def getPacketSize(self):
        return 3

    def populate(self, dataBytes):
        self._value = struct.unpack("H", dataBytes[1:3])[0]

class VelocityPacket:
    def __init__(self):
        self._value = 0.0

    def getPacketSize(self):
        return 3

    def populate(self, dataBytes):
        twoBytes = dataBytes[1:3]
        self._value = struct.unpack("h", twoBytes)

class OneBytePacket:
    def __init__(self):
        self._value = 0

    def getPacketSize(self):
        return 2
    
    def getValue(self):
        return self._value

    def populate(self, dataBytes):
        self._value = ord(dataBytes[1:2])

class StreamPacketFactory:
    def __init__(self, robotState):
        self._robotState = robotState
        self._subscriberList = []

    def registerSubscriber(self, subscriber):
        self._subscriberList.append(subscriber)

    def notify(self):
        for subscriber in self._subscriberList:
            subscriber.notify(self._robotState)

    # backdoor for simulation robot. It helps prevent the need
    # to construct a realistic packet stream just to convert it
    # into a robot state. It is easier for the simulator proxy
    # to just populate the robot state structure.
    def setRobotState(self, robotState):
        self._robotState = robotState

    def getRobotState(self):
        return self._robotState

    # handles next packet in byteStream
    # returns size of packet
    def handleStreamPacket(self, packetStream):
        packetId = ord(packetStream[0])
        if (packetId == PacketId.group0):
            packet = Group0Packet()
            packet.populate(packetStream)

            self._robotState._wheelDrop._caster = packet._wheelDropCaster
            self._robotState._wheelDrop._left = packet._wheelDropLeft
            self._robotState._wheelDrop._right = packet._wheelDropRight
            self._robotState._bump._left = packet._wheelBumpLeft
            self._robotState._bump._right = packet._wheelBumpRight
            self._robotState._cliff._left = packet._cliffLeft
            self._robotState._cliff._right = packet._cliffRight
            self._robotState._cliff._frontLeft = packet._cliffFrontLeft
            self._robotState._cliff._frontRight = packet._cliffFrontRight
            return packet.getPacketSize()

        elif (packetId == PacketId.bumpsAndWheelDrops):
            packet = BumpAndDropPacket()
            packet.populate(packetStream)

            self._robotState._wheelDrop._caster = packet._wheelDropCaster
            self._robotState._wheelDrop._left = packet._wheelDropLeft
            self._robotState._wheelDrop._right = packet._wheelDropRight
            self._robotState._bump._left = packet._wheelBumpLeft
            self._robotState._bump._right = packet._wheelBumpRight
            return packet.getPacketSize()

        elif (packetId == PacketId.batteryCharge):
            packet = BatteryPacket()
            packet.populate(packetStream)
            self._robotState._battery._charge = packet._value
            return packet.getPacketSize()

        elif (packetId == PacketId.batteryCapacity):
            packet = BatteryPacket()
            packet.populate(packetStream)
            self._robotState._battery._capacity = packet._value
            return packet.getPacketSize()

        elif (packetId == PacketId.requestedLeftVelocity):
            packet = VelocityPacket()
            packet.populate(packetStream)
            self._robotState._wheelVelocity._left = packet._value
            return packet.getPacketSize()

        elif (packetId == PacketId.requestedRightVelocity):
            packet = VelocityPacket()
            packet.populate(packetStream)
            self._robotState._wheelVelocity._right = packet._value
            return packet.getPacketSize()

        elif (packetId == PacketId.requestedVelocity):
            packet = VelocityPacket()
            packet.populate(packetStream)
            self._robotState._wheelVelocity._overall = packet._value
            return packet.getPacketSize()

        elif (packetId == PacketId.chargingState):
            packet = OneBytePacket()
            packet.populate(packetStream)
            self._robotState._battery._chargingState = packet.getValue()
            return packet.getPacketSize()

        elif (packetId == PacketId.oiMode):
            packet = OneBytePacket()
            packet.populate(packetStream)
            self._robotState._mode._oiMode = packet.getValue()
            return packet.getPacketSize()
        return 0

    # handles all packets in byteStream
    def streamUpdate(self, byteStream):
        index = 0
        # sum of all packets minus the checksum
        length = len(byteStream) - 1
        while (length != 0 and index < length):
            # slice bytes from index to end
            packetStream = byteStream[index:]
            #print "Remaining Packet: %s"%repr(packetStream)
            #print "Packet Length:", len(packetStream)
            index = index + self.handleStreamPacket(packetStream)

        if (length != 0):
            self.notify()

