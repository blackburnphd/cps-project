#!/usr/bin/env python
import rospy
import math
import simRobotFunctions
from std_msgs.msg import String
from create_2.srv import *
from create_2.msg import *
from gazebo_msgs.msg import ModelState

def callback(data):
    rospy.loginfo(rospy.get_caller_id() + "The message being sent to Gazebo is %s", data.data)
    
def commandListener():
    rospy.Subscriber('gazebo/set_model_state', ModelState, callback)
    # rospy.init_node('listener', anonymous=True)
    # rospy.spin()

if __name__ == '__main__':
    commandListener()