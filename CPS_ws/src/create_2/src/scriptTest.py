#!/usr/bin/env python
import rospy

class logSender:
    def __init__(self):
        rospy.init_node("logSender")
        for x in range(100):
            if x % 5 == 0:
                rospy.loginfo("Info: %s"%(x))
            elif x % 4 == 0:
                rospy.logdebug("Debug: %s"%(x))
            elif x % 3 == 0:
                rospy.logwarn("Warn: %s"%(x))
            elif x % 2 == 0:
                rospy.logerr("Error: %s"%(x))
            else:
                rospy.logfatal("Fatal: %s"%(x))

ls = logSender()
