#!/usr/bin/env python
import rospy
import math
import time
import tf
from std_msgs.msg import String
from gazebo_msgs.msg import ModelState
from gazebo_msgs.msg import ModelStates
from gazebo_msgs.msg import LinkState
from gazebo_msgs.srv import *
from geometry_msgs.msg import *

def driveWheels(rightWheel, leftWheel):
    #Set up publisher to send a message to Gazebo
    model_name = 'create'
    right_link_name = 'create::right_wheel'
    left_link_name = 'create::left_wheel'
    pub = rospy.Publisher('gazebo/set_link_state', LinkState, queue_size=10)
    linkStateArray = getLinkStates(right_link_name, left_link_name)
    modelStateArray = getRobotPosition(model_name)
    rospy.loginfo("Current link state array " + str(linkStateArray))
    if not linkStateArray:
        return
    if not modelStateArray:
        return
    #Find out the robot's orientation to show which direction is "forward"
    modelState = ModelState()
    modelState.pose.orientation.x = modelStateArray[2][0]
    modelState.pose.orientation.y = modelStateArray[2][1]
    modelState.pose.orientation.z = modelStateArray[2][2]
    modelState.pose.orientation.w = modelStateArray[2][3]
    modelOrientationQuat = (modelStateArray[2][0], modelStateArray[2][1], modelStateArray[2][2], modelStateArray[2][3])
    rightAdjusted = quadrantAdjust(modelOrientationQuat, rightWheel)
    leftAdjusted = quadrantAdjust(modelOrientationQuat, leftWheel)
    #
    #Set the message data with the current position and orientation of the robot 
    #and a user-input rotation speed
    msgRight = LinkState()
    msgRight.link_name = linkStateArray[0][0]
    msgRight.pose.orientation.x = linkStateArray[0][2][0]
    msgRight.pose.orientation.y = linkStateArray[0][2][1]
    msgRight.pose.orientation.z = linkStateArray[0][2][2]
    msgRight.pose.orientation.w = linkStateArray[0][2][3]
    msgRight.twist.angular.x = rightAdjusted[0] * (-1)
    msgRight.twist.angular.y = rightAdjusted[1] * (-1)
    msgRight.reference_frame = right_link_name
    rospy.loginfo("Right wheel command " + str(msgRight))
    pub.publish(msgRight)
    #
    msgLeft = LinkState()
    msgLeft.link_name = linkStateArray[1][0]
    msgLeft.pose.orientation.x = linkStateArray[1][2][0]
    msgLeft.pose.orientation.y = linkStateArray[1][2][1]
    msgLeft.pose.orientation.z = linkStateArray[1][2][2]
    msgLeft.pose.orientation.w = linkStateArray[1][2][3]
    msgLeft.twist.angular.x = leftAdjusted[0] * (-1)
    msgLeft.twist.angular.y = leftAdjusted[1] * (-1)
    msgLeft.reference_frame = left_link_name
    rospy.loginfo("Left wheel command " + str(msgLeft))
    pub.publish(msgLeft)

def driveRobot(velocity, angle):
    #Set up publisher to send a message to Gazebo
    model_name = 'create'
    link_name = 'create::base'
    pub = rospy.Publisher('gazebo/set_model_state', ModelState, queue_size=10)
    posArray = getRobotPosition(model_name)
    rospy.loginfo("Current model state array " + str(posArray))
    if not posArray:
        return
    #Set the message data with the current position and orientation of the robot 
    #and a user-input velocity and angle
    msg = ModelState()
    msg.model_name = posArray[0]
    msg.twist.linear.x = velocity
    msg.twist.angular.z = angle
    msg.reference_frame = link_name
    rospy.loginfo("Model command " + str(msg))
    pub.publish(msg)

def getLinkStates(link_name_right, link_name_left):
    #Initiate an array that holds the state of the links
    rightNameAr = link_name_right
    rightPosAr = [0, 0, 0]
    rightOriAr = [0, 0, 0, 0]
    rightLinAr = [0, 0, 0]
    rightAngAr = [0, 0, 0]
    rightStates = [rightNameAr, rightPosAr, rightOriAr, rightLinAr, rightAngAr]
    leftNameAr = link_name_left
    leftPosAr = [0, 0, 0]
    leftOriAr = [0, 0, 0, 0]
    leftLinAr = [0, 0, 0]
    leftAngAr = [0, 0, 0]
    leftStates = [leftNameAr, leftPosAr, leftOriAr, leftLinAr, leftAngAr]
    states = [rightStates, leftStates]
    #Wait for the GetModelState service
    rospy.wait_for_service('/gazebo/get_link_state')
    gms = rospy.ServiceProxy('/gazebo/get_link_state', GetLinkState)
    try:
        #Call the service function and set the states array with the state of the robot
        responseRight = gms(link_name_right, link_name_right)
        states[0][0] = link_name_right
        states[0][1][0] = responseRight.link_state.pose.position.x
        states[0][1][1] = responseRight.link_state.pose.position.y
        states[0][1][2] = responseRight.link_state.pose.position.z
        states[0][2][0] = responseRight.link_state.pose.orientation.x
        states[0][2][1] = responseRight.link_state.pose.orientation.y
        states[0][2][2] = responseRight.link_state.pose.orientation.z
        states[0][2][3] = responseRight.link_state.pose.orientation.w
        states[0][3][0] = responseRight.link_state.twist.linear.x
        states[0][3][1] = responseRight.link_state.twist.linear.y
        states[0][3][2] = responseRight.link_state.twist.linear.z
        states[0][4][0] = responseRight.link_state.twist.angular.x
        states[0][4][1] = responseRight.link_state.twist.angular.y
        states[0][4][2] = responseRight.link_state.twist.angular.z
        #
        responseLeft = gms(link_name_left, link_name_left)
        states[1][0] = link_name_left
        states[1][1][0] = responseLeft.link_state.pose.position.x
        states[1][1][1] = responseLeft.link_state.pose.position.y
        states[1][1][2] = responseLeft.link_state.pose.position.z
        states[1][2][0] = responseLeft.link_state.pose.orientation.x
        states[1][2][1] = responseLeft.link_state.pose.orientation.y
        states[1][2][2] = responseLeft.link_state.pose.orientation.z
        states[1][2][3] = responseLeft.link_state.pose.orientation.w
        states[1][3][0] = responseLeft.link_state.twist.linear.x
        states[1][3][1] = responseLeft.link_state.twist.linear.y
        states[1][3][2] = responseLeft.link_state.twist.linear.z
        states[1][4][0] = responseLeft.link_state.twist.angular.x
        states[1][4][1] = responseLeft.link_state.twist.angular.y
        states[1][4][2] = responseLeft.link_state.twist.angular.z
        return states
    except rospy.ServiceException, e:
        print "Get link states failed: %s"%e

def getRobotPosition(model_name):
    #Initiate an array that holds the state of the robot
    nameAr = model_name
    posAr = [0, 0, 0]
    oriAr = [0, 0, 0, 0]
    linAr = [0, 0, 0]
    angAr = [0, 0, 0]
    states = [nameAr, posAr, oriAr, linAr, angAr]
    #Wait for the GetModelState service
    rospy.wait_for_service('/gazebo/get_model_state')
    gms = rospy.ServiceProxy('/gazebo/get_model_state', GetModelState)
    try:
        #Call the service function and set the states array with the state of the robot
        response = gms(model_name, "world")
        states[0] = model_name
        states[1][0] = response.pose.position.x
        states[1][1] = response.pose.position.y
        states[1][2] = response.pose.position.z
        states[2][0] = response.pose.orientation.x
        states[2][1] = response.pose.orientation.y
        states[2][2] = response.pose.orientation.z
        states[2][3] = response.pose.orientation.w
        states[3][0] = response.twist.linear.x
        states[3][1] = response.twist.linear.y
        states[3][2] = response.twist.linear.z
        states[4][0] = response.twist.angular.x
        states[4][1] = response.twist.angular.y
        states[4][2] = response.twist.angular.z
        return states
    except rospy.ServiceException, e:
        print "Get robot position failed: %s"%e

def reset(model_name):
    #Reset the robot's position, orientation, velocity, and rotation to 0
    pub = rospy.Publisher('gazebo/set_model_state', ModelState, queue_size=10)
    msg = ModelState()
    msg.model_name = model_name
    msg.pose.position.x = 0
    msg.pose.position.y = 0
    msg.pose.position.z = 0
    msg.pose.orientation.x = 0
    msg.pose.orientation.y = 0
    msg.pose.orientation.z = 0
    msg.pose.orientation.w = 0
    msg.twist.linear.x = 0
    msg.twist.linear.y = 0
    msg.twist.linear.z = 0
    msg.twist.angular.x = 0
    msg.twist.angular.y = 0
    msg.twist.angular.z = 0
    msg.reference_frame = 'world'
    rospy.loginfo("Reset model state " + str(msg))
    pub.publish(msg)

def stop():
    #Stop the robot's movement
    driveRobot(0, 0)

def quadrantAdjust(orientationQuat, velocity):
    #Adjusted velocity is in the form [x, y]
    adjustedVelocity = [0, 0]
    orientationEuler = tf.transformations.euler_from_quaternion(orientationQuat)
    orientationZ = orientationEuler[2]
    if (orientationZ > 0 and orientationZ < (math.pi / 2)):
        adjustedVelocity[0] = velocity
        adjustedVelocity[1] = velocity * (-1)
    elif (orientationZ > (math.pi / 2) and orientationZ < math.pi):
        adjustedVelocity[0] = velocity
        adjustedVelocity[1] = velocity
    elif (orientationZ < 0 and orientationZ > ((math.pi / 2) * (-1))):
        adjustedVelocity[0] = velocity * (-1)
        adjustedVelocity[1] = velocity * (-1)
    elif (orientationZ < ((math.pi / 2) * (-1)) and orientationZ > (math.pi * (-1))):
        adjustedVelocity[0] = velocity * (-1)
        adjustedVelocity[1] = velocity
    return adjustedVelocity

# def driveDistance(distance):
#     #Drive the robot a specified distance (meters)
#     #Robot distance is calculated by knowing the speed of the robot
#     velocity = 1
#     waitTime = velocity / distance
#     string = "driveTime = " + str(waitTime)
#     rospy.loginfo(string)
#     driveRobot(velocity, 0)
#     time.sleep(waitTime)
#     stop()

# def rotateAngle(currentAngle, angleToRotate, direction):
#     #Rotate the robot a specified angle (radians)
#     #Direction is either clockwise (negative), or counterclockwise (positive)
#     model_name = "create"
#     driveRobot(0, direction)
#     angleRotated = 0
#     prevAngle = currentAngle
#     while (math.fabs(angleRotated) < angleToRotate):
#         posArray = getRobotPosition(model_name)
#         xQuat = posArray[2][0]
#         yQuat = posArray[2][1]
#         zQuat = posArray[2][2]
#         wQuat = posArray[2][3]
#         orientationQuat = (xQuat, yQuat, zQuat, wQuat)
#         orientationEuler = tf.transformations.euler_from_quaternion(orientationQuat)
#         currentAngle = orientationEuler[2]
#         angleRotated += math.fabs(currentAngle - prevAngle)
#         prevAngle = currentAngle
#     stop()
#     rospy.loginfo("Angle rotated " + str(angleRotated))

# def angleToRotate(differenceX, differenceY, distance, currentOrientationZ):
#     rospy.loginfo("currentOrientationZ " + str(currentOrientationZ))
#     #If differenceY < 0, the robot needs to rotate clockwise
#     if (differenceY < 0):
#         rotationDirection = -1
#     else:
#         rotationDirection = 1
#     #
#     #The desired orientation relative to 0 is calculated here
#     desiredOrientationZ = math.acos(differenceY / distance)
#     rospy.loginfo("desiredOrientationZ " + str(desiredOrientationZ))
#     #
#     #If the current and desired orientations are both positive or both negative, rotate (ad - ac)
#     #If the current and desired orientations are not both positive or both negative, rotate (ad + ac)
#     currentOrientationSignCheck = currentOrientationZ / math.fabs(currentOrientationZ)
#     desiredOrientationSignCheck = desiredOrientationZ / math.fabs(desiredOrientationZ)
#     if (currentOrientationSignCheck == desiredOrientationSignCheck):
#         totalAngleToRotate = math.fabs(currentOrientationZ) - math.fabs(desiredOrientationZ) 
#     else:
#         totalAngleToRotate = math.fabs(currentOrientationZ) + math.fabs(desiredOrientationZ) 
#     rospy.loginfo("totalAngleToRotate " + str(totalAngleToRotate))
#     rotationArray = [totalAngleToRotate, rotationDirection]
#     return rotationArray

# def driveToLocation(desiredX, desiredY):
#     #Drive the robot to a specified location at (desiredX, desiredY)
#     #Check the robot's current location and orientation
#     model_name = "create"
#     posArray = getRobotPosition(model_name)
#     currentX = posArray[1][0]
#     currentY = posArray[1][1]
#     xQuat = posArray[2][0]
#     yQuat = posArray[2][1]
#     zQuat = posArray[2][2]
#     wQuat = posArray[2][3]
#     orientationQuat = (xQuat, yQuat, zQuat, wQuat)
#     orientationEuler = tf.transformations.euler_from_quaternion(orientationQuat)
#     orientationZ = orientationEuler[2]
#     #
#     #The triangle created using the robot's coordinates 
#     #relative to the desired location is calculated here
#     differenceX = desiredX - currentX
#     differenceY = desiredY - currentY
#     distance = math.hypot(differenceX, differenceY)
#     # mathArray = coordinateMath(differenceX, )
#     #
#     totalAngleToRotate = angleToRotate(differenceX, differenceY, distance, orientationZ)
#     rotateAngle(orientationZ, totalAngleToRotate[0], totalAngleToRotate[1])
#     driveDistance(distance)
#     #
#     # driveRobot(1, 0)
#     # currentDistance = distance
#     # while (currentDistance > tolerance):
#     #     posArray = getRobotPosition(model_name)
#     #     currentDistanceX = math.fabs(desiredX - posArray[1][0])
#     #     currentDistanceY = math.fabs(desiredY - posArray[1][1])
#     #     currentDistance = math.hypot(currentDistanceX, currentDistanceY)
#     #     # rospy.loginfo(currentDistance)
#     #     # pub.publish(currentDistance)
#     # stop()
#     # msg = "Movement complete"
#     # rospy.loginfo(msg)
#     # pub.publish(msg)