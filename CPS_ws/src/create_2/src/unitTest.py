import rospy
import math
import simRobotFunctions
from std_msgs.msg import String
from create_2.srv import *
from create_2.msg import IntList

# currentAngle = (math.pi) / 2
# currentPosX = 0
# currentPosY = 0
# desiredX = 3
# desiredY = 4
# tolerance = 0.001
# angleToRotate = simRobotFunctions.angleToRotate(currentPosX, currentPosY, currentAngle, desiredX, desiredY)
# rospy.loginfo("angleToRotate = " + str(angleToRotate) + "\n")
# print ("angleToRotate = " + str(angleToRotate))

angle = (math.pi)
direction = 1
time = simRobotFunctions.rotateAngle(angle, direction)
print ("time to rotate = " + str(time))