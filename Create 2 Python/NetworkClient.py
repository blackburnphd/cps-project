import socket
import select
import time

class SocketClient:

    def __init__(self, addr, timeout):
        self._host, self._port = addr
        self._timeout = timeout

        self._s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        print(self._s)

    def connect(self, addr):
        self._s.connect(addr)
        print("client connect")
        self._s.setblocking(0)

    def disconnect(self):
        print("client disconnect")
        self._s.close()

    def blockRead(self, count):
        sent = False
        while (not sent):
            try:
                s = self._s.recv(count)
                sent = True
                #print("client blockRead: read: " + str(s))
                return s
            except socket.error as err:
                time.sleep(0.001)

    def tryRead(self, count):
        try:
            ready = select.select([self._s],[],[],0.5)
            if (ready[0]):
                return self.blockRead(count)
            else:
                return bytes([])
        except socket.error(msg):
            self._s.close()
            print("client tryRead error: "+str(msg[0]))

class NetworkClient(SocketClient):
    def __init__(self):
        TIMEOUT = 0.5
        HOST = 'localhost'
        PORT = 9999
        SocketClient.__init__(self, (HOST, PORT), TIMEOUT)

    def write(self, cmdBytes):
        self._s.sendall(cmdBytes)

