import tkinter as tk
import GUIBase as gb
import RemoteRobotProxy as rrp

class SocketInfo(tk.Frame):
    def __init__(self, master):
        tk.Frame.__init__(self, master, borderwidth=2, relief="groove")

        self._topFrame = tk.Frame(self)
        self._topFrame.pack(side="top")

        self._bottomFrame = tk.Frame(self)
        self._bottomFrame.pack(side="bottom")

        self._robotProxy = robotProxy

        self._addrVar = tk.StringVar()
        self._addrVar.set("localhost")
        self._addrEntry = tk.Entry(self._bottomFrame,
            textvariable=self._addrVar)
        self._addrEntry.pack(side="left")

        self._portVar = tk.StringVar()
        self._portVar.set("9999")
        self._portEntry = tk.Entry(self._bottomFrame,
            textvariable=self._portVar)
        self._portEntry.pack(side="left")

class RemoteClientApp(tk.Frame):

    def __init__(self, robotProxy, master=None):
        tk.Frame.__init__(self, master)
        self.pack()

        self._base = gb.ClientApp(robotProxy, self)
        self._base.pack(side="bottom")

        self._socketInfo = SocketInfo(self)
        self._socketInfo.pack(side="top")

robotProxy = rrp.RemoteRobotProxy()
root = tk.Tk()
#app = gb.ClientApp(robotProxy, master=root)
app = RemoteClientApp(robotProxy, master=root)
robotProxy.setAddr(app._socketInfo._addrVar, 
                   app._socketInfo._portVar)

root.mainloop()
