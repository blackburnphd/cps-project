from enum import IntEnum
import SerialComponent as ser
import StreamPacketFactory as spf
import time

class OpCode(IntEnum):
    # start commands
    start = 128
    baud = 129
    safe = 131
    full = 132
    # demo commands
    demo = 136
    cover = 135
    coverAndDock = 143
    spot = 134
    # actuator commands
    drive = 137
    driveDirect = 145
    leds = 139
    digitalOutputs = 147
    pwmLowSideDrivers = 144
    lowSideDrivers = 138
    sendIr = 151
    song = 140
    playSong = 141
    # input commands
    sensors = 142
    queryList = 149
    stream = 148
    pauseResumeStream = 150
    # script commands
    script = 152
    playScript = 153
    showScript = 154
    # wait commands
    waitTime = 155
    waitDistance = 156
    waitAngle = 157
    waitEvent = 158

