import NetworkClient as net

class RemoteRobotCommand:

    def __init__(self, robotState):
        self._isSocketOpen = 0
        self._net = net.NetworkClient()
        self._subscriberList = []
        self._robotState = robotState
        self._addrVar = None
        self._portVar = None
        self._addr = ("localhost", 9999)
        self._serialPortInfo = "/dev/ttyUSB0"

    def connect(self):
        if (self._addrVar != None and self._portVar != None):
            ip = self._addrVar.get()
            port = int(self._portVar.get())
            self._addr = (ip, port)
            print(self._addr)
        self._net.connect(self._addr)
        self._isConnected = 1

    def disconnect(self):
        self._net.disconnect()
        self._isConnected = 0

    def startStream(self):
        cmd = "startStream " + self._serialPortInfo + " $"
        self._net.write(bytearray(cmd, "utf-8"))

    def readPacket(self):
        foundEnd = False
        readBytes = bytearray([])
        while (not foundEnd):
            oneByte = self._net.blockRead(1)
            if (len(oneByte) > 0):
                oneChar = oneByte.decode("utf-8")
                foundEnd = (oneChar[0] == "$")
                readBytes += oneByte
        return readBytes

    def checkStream(self):
        count = 0
        readBytes = bytearray(self._net.tryRead(1))
        go = (len(readBytes) != 0)
        while (go):
            readBytes += self.readPacket()
            entireString = readBytes.decode("utf-8")
            tokens = entireString.split(" ")
            #print(tokens)
            self._robotState._wheelDrop._caster = int(tokens[1])
            self._robotState._wheelDrop._left = int(tokens[2])
            self._robotState._wheelDrop._right = int(tokens[3])
            self._robotState._bump._left = int(tokens[4])
            self._robotState._bump._right = int(tokens[5])
            self._robotState._cliff._left = int(tokens[6])
            self._robotState._cliff._right = int(tokens[7])
            self._robotState._cliff._frontLeft = int(tokens[8])
            self._robotState._cliff._frontRight = int(tokens[9])
            self._robotState._battery._charge = int(tokens[10])
            self._robotState._battery._capacity = int(tokens[11])
            self._robotState._wheelVelocity._left = int(tokens[12])
            self._robotState._wheelVelocity._right = int(tokens[13])
            self._robotState._wheelVelocity._overall = int(tokens[14])

            # try to read next packet
            readBytes = bytearray(self._net.tryRead(1))
            go = (len(readBytes) != 0)

            # update subscribers with latest data
            if (not go):
                self.notify(self._robotState)

    def drive(self, velocity, radius):
        cmd = "drive " 
        cmd += str(velocity) + " " + str(radius) + " $" 
        print(cmd)
        self._net.write(bytearray(cmd, "utf-8"))

    def driveDirect(self, rightWheelVelocity, leftWheelVelocity):
        cmd = "driveDirect "
        cmd += str(rightWheelVelocity) + " " + str(leftWheelVelocity) + " $"
        print(cmd)
        self._net.write(bytearray(cmd, "utf-8"))

    # whichLED bit field specifies which LEDS to turn on:
    # bit 1 = play, bit 3 = advance (green, full intensity)
    # power LED color: range (0-255), 0 = green, 255 = red
    # power LED intensity: range (0-255), 0 = off, 255 = full intensity
    def setLEDs(self, whichLED, powerColor, powerIntensity):
        cmd = "led " 
        cmd += str(whichLED) + " " + str(powerColor) + " "
        cmd += str(powerIntensity) + " $"
        print(cmd)
        self._net.write(bytearray(cmd, "utf-8"))
            
    def demo(self):
        cmd = "demo "
        print(cmd)
        self._net.write(bytearray(cmd, "utf-8"))
        
    def start(self):
        cmd = "start "
        print(cmd)
        self._net.write(bytearray(cmd, "utf-8"))
        
    def cover(self):
        cmd = "cover "
        print(cmd)
        self._net.write(bytearray(cmd, "utf-8"))
        
    def coverAndDock(self):
        cmd = "coverAndDock " 
        print(cmd)
        self._net.write(bytearray(cmd, "utf-8"))
        
    def spot(self):
        cmd = "spot "
        print(cmd)
        self._net.write(bytearray(cmd, "utf-8"))

    def registerSubscriber(self, subscriber):
        self._subscriberList.append(subscriber)

    def notify(self, robotState):
        for subscriber in self._subscriberList:
            subscriber.notify(robotState)

    def setAddr(self, addrVar, portVar):
        self._addrVar = addrVar
        self._portVar = portVar

    def setSerialPortInfo(self, serialPortString):
        self._serialPortInfo = serialPortString
