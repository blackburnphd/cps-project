import SimRobotProxy as rp
import time

class Subscriber:
    def notify(self, robotState):
        print("updateCallback")
        print("velocity: left, right = ", robotState._wheelVelocity._left,
              robotState._wheelVelocity._right)
        print("drop C, L, R: bump L R: ",
              robotState._wheelDrop._caster,
              robotState._wheelDrop._left,
              robotState._wheelDrop._right,
              robotState._bump._left,
              robotState._bump._right)
        print("cliff: L R FL FR: ",
              robotState._cliff._left,
              robotState._cliff._right,
              robotState._cliff._frontLeft,
              robotState._cliff._frontRight)
        print("battery charge capacity: ",
              robotState._battery._charge,
              robotState._battery._capacity)
              

robotProxy = rp.SimRobotProxy()
sub = Subscriber()
robotProxy.registerSubscriber(sub)

# use for Linux USB-to-serial adapter
#robotProxy.setSerialPortInfo("/dev/ttyUSB0")

# use for Linux serial port
#robotProxy.setSerialPortInfo("/dev/ttyS0")

robotProxy.connect()
time.sleep(0.5)
# check stream at 15 ms intervals
for num in range(1,5):
    time.sleep(0.015)
    #robotCommand.getCliffGroup()

print("aaa start stream aaa")
robotProxy.startStream()
time.sleep(1)
for num in range(1,50):
    time.sleep(1)
    robotProxy.checkStream()
    color = 0
    if (num % 4 == 0):
        color = 0x1
    elif (num % 4 == 1):
        color = 0x1 << 3
    elif (num % 4 == 2):
        color = 0x1 | (0x1 << 3)
    else:
        color = 0
    #robotProxy.setLEDs(color, 254, 100)
    #robotProxy.driveDirect(100, 400)
    robotProxy.drive(400, 3000)

robotProxy.disconnect()
