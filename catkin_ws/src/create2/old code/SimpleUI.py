import LocalRobotProxy as rp
import time

class Subscriber:
    def notify(self, robotState):
        print("updateCallback")
        print("label ", robotState._cliff._frontLeft,
              robotState._cliff._frontRight,
              robotState._cliff._left,
              robotState._cliff._right)

robotProxy = rp.LocalRobotProxy()
sub = Subscriber()
robotProxy.registerSubscriber(sub)

# use for Linux USB-to-serial adapter
robotProxy.setSerialPortInfo("/dev/ttyUSB0")

# use for Linux serial port
#robotProxy.setSerialPortInfo("/dev/ttyS0")

robotProxy.connect()
time.sleep(0.5)
# check stream at 15 ms intervals
for num in range(1,5):
    time.sleep(0.015)
    #robotCommand.getCliffGroup()

print("aaa start stream aaa")
robotProxy.startStream()
time.sleep(1)
for num in range(1,50):
    time.sleep(0.015)
    robotProxy.checkStream()

robotProxy.disconnect()
