#!/usr/bin/env python

import Tkinter as tk
from threading import Thread
import time

class SerialPortInfo(tk.Frame):
    def __init__(self, master):
        tk.Frame.__init__(self, master, borderwidth=2, relief="groove")

        self._leftFrame = tk.Frame(self)
        self._leftFrame.pack(side="left")

        self._rightFrame = tk.Frame(self)
        self._rightFrame.pack(side="right")

        self._portLabel = tk.Label(self._leftFrame, text="serial port")
        self._portLabel.pack(side="right")

        self._portVar = tk.StringVar()
        # Note the default ttyUSB0 is for USB-to-serial adaptor.
        # For machines that use the serial port try using "/dev/ttyS0".
        self._portVar.set("/dev/ttyUSB0")
        self._portEntry = tk.Entry(self._rightFrame,
            textvariable=self._portVar)
        self._portEntry.pack(side="right")

class ConnectButton(tk.Button):
    def __init__(self, master, tick, guiClient, serialPortVar):
        tk.Button.__init__(self, master,
            text="Connect", 
            command=self.buttonCallback)
        self._guiClient = guiClient
        self._tickLabel = tick
        self._serialPortVar = serialPortVar

    def buttonCallback(self):
        self._guiClient.connect(self._serialPortVar.get())
        
        self._tickLabel.tick()

#def connect(self):
    #    connected = self._guiClient.connect(self._serialPortVar.get())
    #    if connected == True:
    #        self.config(text = "Disconnect", command = self.disconnect)
    #        self._tickLabel.tick()
    #    else:
    #        print("Connect request to %s failed"%(self._serialPortVar.get()))

    #def disconnect(self):
    #    disconnected = self._guiClient.connect(0)
    #    if disconnected == True:
    #        self.config(text = "Connect", command = self.connect)
    #    else:
    #        print ("Disconnect request to %s failed"&(self._serialPortVar.get()))

#class RefreshButton(tk.Button):
#    def __init__(self, master, tick, guiClient):
#        tk.Button.__init__(self, master, text="check stream", command=self.buttonCallback)
#        self._guiClient = guiClient
#        self._tickLabel = tick

#    def buttonCallback(self):
#        self._tickLabel.tick()

class TickLabel(tk.Label):
    def __init__(self, master, guiClient):
        self.v = tk.StringVar()
        tk.Label.__init__(self, master, textvariable=self.v)
        self._timeIncrement = 0
        self._guiClient = guiClient

    def tick(self):
        self._timeIncrement += 1
        self.v.set(self._timeIncrement)
        #self._guiClient.getRobotState
        # serial refreshes at 15 ms intervals
        self.after(30, self.tick)

class StatusLabel(tk.Label):
    def __init__(self, master, guiClient):
        self.v = tk.StringVar()
        tk.Label.__init__(self, master, textvariable=self.v)
        self.exitFlag = False
        self.buildString(guiClient.getRobotState())

    def notify(self, robotState):
        self.buildString(robotState)

    def close(self):
        self.exitFlag = True

class Battery(StatusLabel):
    def __init__(self, master, robotState):
        StatusLabel.__init__(self, master, robotState)

    def buildString(self, robotState):
        chargingState = robotState._battery._chargingState
        if chargingState == 0:
            state = "Not Charging"
        elif chargingState == 1:
            state = "Reconditioning Charging"
        elif chargingState == 2:
            state = "Full Charging"
        elif chargingState == 3:
            state = "Trickle Charging"
        elif chargingState == 4:
            state = "Waiting"
        elif chargingState == 5:
            state = "Charging Fault Condition"
        text = "Battery Charge %d\nBattery Capacity %d\nCharging State: %s" % (
            robotState._battery._charge,
            robotState._battery._capacity,
            state)
        if not self.exitFlag:
            self.v.set(text)

class Mode(StatusLabel):
    def __init__(self, master, robotState):
        StatusLabel.__init__(self, master, robotState)

    def buildString(self, robotState):
        mode = robotState._mode._oiMode
        if mode == 0:
            state = "Off"
        elif mode == 1:
            state = "Passive"
        elif mode == 2:
            state = "Safe"
        elif mode == 3:
            state = "Full"
        text = "OI Mode: %s"%(state)
        if not self.exitFlag:
            self.v.set(text)

class WheelVelocity(StatusLabel):
    def __init__(self, master, guiClient):
        StatusLabel.__init__(self, master, guiClient)

    def buildString(self, robotState):
        text = "Wheel Velocity: R %d, L %d\n Overall Velocity: %d" % (
            robotState._wheelVelocity._right, 
            robotState._wheelVelocity._left,
            robotState._wheelVelocity._overall)
        if not self.exitFlag:
            self.v.set(text)

class WheelDrop(StatusLabel):
    def __init__(self, master, guiClient):
        self._timer = 0
        StatusLabel.__init__(self, master, guiClient)

    def buildString(self, robotState):
        self._timer += 1
        #self["text"] = "Wheel Drop: C %d, L %d, R %d" % (
            #robotState._wheelDrop._caster, 
            #robotState._wheelDrop._left,
            #robotState._wheelDrop._right)
        print("Wheel Drop")

class Cliff(StatusLabel):
    def __init__(self, master, guiClient):
        StatusLabel.__init__(self, master, guiClient)

    def buildString(self, robotState):
        #self["text"] = "Cliff: FL %d, FR %d, L %d, R %d" % (
            #robotState._cliff._frontLeft, 
            #robotState._cliff._frontRight,
            #robotState._cliff._left,
            #robotState._cliff._right)
        print("Cliff")

class Bumpers(StatusLabel):
    def __init__(self, master, guiClient):
        StatusLabel.__init__(self, master, guiClient)

    def buildString(self, robotState):
        #self["text"] = "Bumps: L %d, R %d" % (
            #robotState._bump._left, 
            #robotState._bump._right)
        print("Bump")

class SensorsFrame(tk.Frame):
    def __init__(self, master, guiClient):
        tk.Frame.__init__(self, master)

        self.exitFlag = False

        self.wheelVelocity = WheelVelocity(self, guiClient)
        self.wheelVelocity.pack(side="bottom")
        
        self.battery = Battery(self, guiClient)
        self.battery.pack(side="bottom")
        
        self.mode = Mode(self, guiClient)
        self.mode.pack(side="bottom")
        
        #self.wheelDrop = WheelDrop(self, guiClient)
        #self.wheelDrop.pack(side="bottom")
        
        #self.cliff = Cliff(self, guiClient)
        #self.cliff.pack(side="bottom")
        
        #self.bumpers = Bumpers(self, guiClient)
        #self.bumpers.pack(side="bottom")

    def update(self, robotState):
        children = self.winfo_children()
        for child in children:
            if self.exitFlag:
                break
            child.buildString(robotState)

    def close(self):
        self.exitFlag = True

class CommandLED(tk.Frame):
    def __init__(self, master, guiClient):
        tk.Frame.__init__(self, master, borderwidth=2, relief="groove")

        self._topFrame = tk.Frame(self)
        self._topFrame.pack(side="top")

        self._bottomFrame = tk.Frame(self)
        self._bottomFrame.pack(side="bottom")

        self._guiClient = guiClient

        self._LEDBtn = tk.Button(self._topFrame,
            text="LED(whichLEDs,color,intensity)",
            command=self.buttonCallback)
        self._LEDBtn.pack(side="left")

        optionList = ("none", "play", "adv", "both")
        self._option = tk.StringVar()
        self._option.set(optionList[0])
        self._optionMenu = tk.OptionMenu(self._topFrame, self._option,
            *optionList)
        self._optionMenu.pack(side="right")

        self._colorVar = tk.StringVar()
        self._colorVar.set("0")
        self._colorEntry = tk.Entry(self._bottomFrame, 
            textvariable=self._colorVar)
        self._colorEntry.pack(side="left")

        self._intensityVar = tk.StringVar()
        self._intensityVar.set("0")
        self._intensityEntry = tk.Entry(self._bottomFrame, 
            textvariable=self._intensityVar)
        self._intensityEntry.pack(side="left")

    def buttonCallback(self):
        optString = self._option.get()
        whichLED = 0 # none
        if (optString == "play"):
            whichLED = 2
        elif (optString == "adv"):
            whichLED = 8
        elif (optString == "both"):
            whichLED = 2 | 8 # both
        color = int(self._colorVar.get())
        intensity = int(self._intensityVar.get())
        self._guiClient.setLEDs(whichLED, color, intensity)

class CommandDrive(tk.Frame):
    def __init__(self, master, guiClient):
        tk.Frame.__init__(self, master, borderwidth=2, relief="groove")

        self._driveBtn = tk.Button(self,
            text="drive(velocity,radius)",
            command=self.buttonCallback)
        self._driveBtn.pack(side="top", fill='x')

        vFrame = tk.Frame(self)
        vFrame.pack(side="top")
        rFrame = tk.Frame(self)
        rFrame.pack(side="top")

        self._guiClient = guiClient

        self._velocityVar = tk.StringVar()
        self._velocityVar.set("0")
        vLabel = tk.Label(vFrame, text="Velocity (mm/s):", anchor="e", width=15)
        vLabel.pack(side="left")
        self._velocityEntry = tk.Entry(vFrame, width=6, textvariable=self._velocityVar)
        self._velocityEntry.pack(side="left")
        self._vScale = tk.Scale(vFrame, from_=-500, to=500, length=150, orient=tk.HORIZONTAL, showvalue=0, variable = self._velocityVar)
        self._vScale.pack(side="left", fill='x')

        self._radiusVar = tk.StringVar()
        self._radiusVar.set("0")
        rLabel = tk.Label(rFrame, text="Radius (mm):", anchor="e", width=15)
        rLabel.pack(side="left")
        self._radiusEntry = tk.Entry(rFrame, width=6, textvariable=self._radiusVar)
        self._radiusEntry.pack(side="left")
        self._rScale = tk.Scale(rFrame, from_=-2000, to=2000, length=150, orient=tk.HORIZONTAL, showvalue=0, variable = self._radiusVar)
        self._rScale.pack(side="left", fill='x')

    def buttonCallback(self, arg=0):
        velocity = int(self._velocityVar.get())
        radius = int(self._radiusVar.get())
        self._guiClient.drive(velocity, radius)

class CommandDriveDirect(tk.Frame):
    def __init__(self, master, guiClient):
        tk.Frame.__init__(self, master, borderwidth=2, relief="groove")

        self._driveBtn = tk.Button(self,
            text="driveDirect(left velocity,right velocity)",
            command=self.buttonCallback)
        self._driveBtn.pack(side="top", fill='x')

        v1Frame = tk.Frame(self)
        v1Frame.pack(side="top")
        v2Frame = tk.Frame(self)
        v2Frame.pack(side="top")

        self._guiClient = guiClient

        self._leftVelocityVar = tk.StringVar()
        self._leftVelocityVar.set("0")
        v1Label = tk.Label(v1Frame, text="Left Wheel (mm/s):", anchor="e", width=16)
        v1Label.pack(side="left")
        v1Entry = tk.Entry(v1Frame, width=6, textvariable=self._leftVelocityVar)
        v1Entry.pack(side="left")
        v1Scale = tk.Scale(v1Frame, from_=-500, to=500, length=150, orient=tk.HORIZONTAL, showvalue=0, variable = self._leftVelocityVar)
        v1Scale.pack(side="left", fill='x')

        self._rightVelocityVar = tk.StringVar()
        self._rightVelocityVar.set("0")
        v2Label = tk.Label(v2Frame, text="Right Wheel (mm/s):", anchor="e", width=16)
        v2Label.pack(side="left")
        v2Entry = tk.Entry(v2Frame, width=6, textvariable=self._rightVelocityVar)
        v2Entry.pack(side="left")
        v2Scale = tk.Scale(v2Frame, from_=-500, to=500, length=150, orient=tk.HORIZONTAL, showvalue=0, variable = self._rightVelocityVar)
        v2Scale.pack(side="left", fill='x')

    def buttonCallback(self):
        rightVelocity = int(self._rightVelocityVar.get())
        leftVelocity = int(self._leftVelocityVar.get())
        self._guiClient.driveDirect(rightVelocity, leftVelocity)

class CommandStopMoving(tk.Button):
    def __init__(self, master, guiClient):
        tk.Button.__init__(self, master,
            text="Abort",
            command=self.buttonCallback)
        self._guiClient = guiClient

    def buttonCallback(self):
        self._guiClient.abort()

class ClientApp(tk.Frame):
    def __init__(self, guiClient, master=None):
        tk.Frame.__init__(self, master)
        self.exitFlag = False
        self.pack()

        self._guiClient = guiClient

        # Menu bar

        menubar = tk.Menu(self)

        fileMenu = tk.Menu(menubar, tearoff=0)
        fileMenu.add_command(label="Exit", command=self.close)
        menubar.add_cascade(label="File", menu=fileMenu)

        powerMenu = tk.Menu(menubar, tearoff=0)
        powerMenu.add_command(label="Start", command=lambda:self.powerCommand("start"))
        powerMenu.add_command(label="Stop", command=lambda:self.powerCommand("stop"))
        powerMenu.add_command(label="Reset", command=lambda:self.powerCommand("reset"))
        menubar.add_cascade(label="Power", menu=powerMenu)

        demoMenu = tk.Menu(menubar, tearoff=0)
        demoMenu.add_command(label="Max Clean", command=lambda:self._guiClient.demo("max"))
        demoMenu.add_command(label="Clean", command=lambda:self._guiClient.demo("clean"))
        demoMenu.add_command(label="Seek Dock", command=lambda:self._guiClient.demo("dock"))
        demoMenu.add_command(label="Spot Clean", command=lambda:self._guiClient.demo("spot"))
        menubar.add_cascade(label="Demo", menu=demoMenu)

        master.config(menu=menubar)

        # End menu bar

        self._connectFrame = tk.Frame(self)
        self._connectFrame.pack(side="top", fill='x')

        self._stop = CommandStopMoving(self, self._guiClient)
        self._stop.pack(side="bottom", fill='x')

        modeList = ["off", "passive", "safe", "full"]
        self.currentMode = tk.StringVar()
        self.currentMode.set(modeList[0])
        self._modeMenu = tk.OptionMenu(self._connectFrame, self.currentMode, *modeList, command=self.setMode)
        self._modeMenu.pack(side="top")

        self._tick = TickLabel(self, self._guiClient)
        self._tick.pack(side="bottom")

        self._serialPortInfo = SerialPortInfo(self._connectFrame)
        self._connect = ConnectButton(self._connectFrame, self._tick, 
            self._guiClient, self._serialPortInfo._portVar)

        self._connect.pack()
        self._serialPortInfo.pack()

        self._commandLED = CommandLED(self, self._guiClient)
        self._commandLED.pack(side="bottom")

        #self._commandDemo = CommandDemo(self, self._guiClient)
        #self._commandDemo.pack(side="bottom")

        self._commandDrive = CommandDrive(self, self._guiClient)
        self._commandDrive.pack(side="bottom", fill='x')

        self._commandDrive = CommandDriveDirect(self, self._guiClient)
        self._commandDrive.pack(side="bottom", fill='x')

        self._sensorsFrame = SensorsFrame(self, 
            self._guiClient)
        self._sensorsFrame.pack(side="bottom")

        self._resetModel = tk.Button(self, text="Reset Model", command = self.resetModel)
        self._resetModel.pack(side="bottom")

        # Update text fields
        self.updateSensorsThread = Thread(name='daemon', target = self.updateText)
        self.updateSensorsThread.start()
        
    # Loop to update text fields
    def updateText(self):
        while not self.exitFlag:
            robotState = self._guiClient.getRobotState()
            self._sensorsFrame.update(robotState)
            time.sleep(0.5)

    def stateChange(self, robotState):
        print("stateChange")

    # Start robot, power it off, or restart it
    def powerCommand(self, command):
        self._guiClient.powerCommand(command)

    # Set mode to Off, Passive, Safe, or Full
    def setMode(self, mode):
        self._guiClient.setMode(mode)

    # Reset Gazebo model
    def resetModel(self):
        self._guiClient.resetModel()

    # Processing on close
    def close(self):
        self.exitFlag = True
        self._sensorsFrame.close()
        self.updateSensorsThread.join()
        self._guiClient.close()
        print "GUIBase closed"
        time.sleep(0.5)
        self.winfo_toplevel().destroy()

def updateCallback(robotState):
    app.stateChange(robotState)
