import SerialComponent as ser
import StreamPacketFactory as spf
import time
import RobotCommand as rc
import struct

class CommandSeq:
    def send(self, serial):
        #print("send: ", bytearray(self.getCommand()))
        serial.write(bytearray(self.getCommand()))

class StartCommand(CommandSeq):
    def getCommand(self):
        return [ rc.OpCode.start ]

class ResetCommand(CommandSeq):
    def getCommand(self):
        return [ rc.OpCode.reset ]

class PauseResumeCommand(CommandSeq):
    def __init__(self, resumeOn):
        self._resumeOn = resumeOn

    def getCommand(self):
        return [ rc.OpCode.pauseResumeStream, self._resumeOn ]

class StreamCommand(CommandSeq):
    def __init__(self):
        self._cmd = [ rc.OpCode.stream, 0 ]

    def addPacket(self, packetId):
        self._cmd[1] = self._cmd[1] + 1
        self._cmd.append(packetId)

    def getCommand(self):
        #print("stream: ", self._cmd)
        return self._cmd

class QueryCommand(CommandSeq):
    def __init__(self):
        self._cmd = [rc.OpCode.queryList, 0]

    def addPacket(self, packetId):
        self._cmd[1] = self._cmd[1] + 1
        self._cmd.append(packetId)

    def getCommand(self):
        #print("query: ", self._cmd)
        return self._cmd

class SensorsCommand(CommandSeq):
    def __init__(self, packetId):
        self._packetId = packetId

    def getCommand(self):
        return [ rc.OpCode.sensors, self._packetId ]
        
class LocalRobotCommand:

    def __init__(self, streamPacketFactory):
        self._isSerialOpen = 0
        self._streamPacketFactory = streamPacketFactory
        self._serial = ser.SerialComponent()
        self._serialPortInfo = "/dev/ttyUSB0"
        pass

    def connect(self):
        self._serial.open(self._serialPortInfo)
        self._isSerialOpen = 1

    def disconnect(self):
        self._serial.close()
        self._isSerialOpen = 0

    def send(self, commandSeq):
        return commandSeq.send(self._serial)

    def start(self):
        self.send(StartCommand())
        time.sleep(0.5)

    def startStream(self):
        strCmd = StreamCommand()
        #strCmd.addPacket(spf.PacketId.group0)
        strCmd.addPacket(spf.PacketId.requestedVelocity)
        strCmd.addPacket(spf.PacketId.requestedRightVelocity)
        strCmd.addPacket(spf.PacketId.requestedLeftVelocity)
        strCmd.addPacket(spf.PacketId.batteryCapacity)
        strCmd.addPacket(spf.PacketId.batteryCharge)
        strCmd.addPacket(spf.PacketId.chargingState)
        strCmd.addPacket(spf.PacketId.oiMode)
        self.send(strCmd)

    def checkStream(self):
        count = 0
        readBytes = self._serial.tryRead(1)
        # readBytes represents bytes from the serial port
        # If you print it, it looks like a box. Doing repr(readBytes) returns
        # a readable version of the code, which looks like hex. It can't be converted
        # Ord() actually translates the bytes to decimal
        go = (len(readBytes) != 0)
        while (go):
            if (ord(readBytes[0]) == spf.PacketId.streamPacket):
                # number of bytes between n-bytes byte and checksum 
                numBytes = ord(self._serial.blockRead(1))
                # adding one to get the trailing checksum
                packetBytes = self._serial.blockRead(numBytes)
                # Assumes we already found the packet marker ('19')
                # and the N-bytes byte.
                # Send to streamPacketFactory to decode packets and
                # notify subscribers.
                self._streamPacketFactory.streamUpdate(packetBytes)
            readBytes = self._serial.tryRead(1)
            go = (len(readBytes) != 0)

    def drive(self, velocity, radius):
        if velocity < -500:
            velocity = -500
        elif velocity > 500:
            velocity = 500
        if radius < -2000:
            radius = -2000
        elif radius > 2000:
            radius = 2000

        self._serial.write(bytearray([rc.OpCode.full]))
        time.sleep(0.1)

        v = int(velocity) & 0xffff
        r = int(radius) & 0xffff
        #data = struct.unpack('4B', struct.pack('>2H', v, r))
        data = struct.pack('>2H',v,r)
        driveCommand = bytearray([rc.OpCode.drive])
        byteStr = bytearray(data)
        self._serial.write(driveCommand)
        self._serial.write(byteStr)

    def driveDirect(self, v_r, v_l):       
        if v_r < -500:
            v_r = -500
        elif v_r > 500:
            v_r = 500
        if v_l < -500:
            v_l = -500
        elif v_l > 500:
            v_l = 500

        self._serial.write(bytearray([rc.OpCode.full]))
        time.sleep(0.1)

        v_r = int(v_r) & 0xffff
        v_l = int(v_l) & 0xffff
        data = struct.pack('>2H',v_r,v_l)
        driveCommand = bytearray([rc.OpCode.driveDirect])
        byteStr = bytearray(data)
        self._serial.write(driveCommand)
        self._serial.write(byteStr)

    # whichLED bit field specifies which LEDS to turn on:
    # bit 1 = play, bit 3 = advance (green, full intensity)
    # power LED color: range (0-255), 0 = green, 255 = red
    # power LED intensity: range (0-255), 0 = off, 255 = full intensity
    def setLEDs(self, whichLED, powerColor, powerIntensity):
        command = [rc.OpCode.full, 
            rc.OpCode.leds, whichLED, powerColor, powerIntensity]
        #print(command)
        byteStr = bytearray(command)
        self._serial.write(byteStr)

    def setSerialPortInfo(self, serialPortString):
        self._serialPortInfo = serialPortString

    def cleanCommand(self, demo):
        if demo == "clean":
            command = [rc.OpCode.cover]
            byteStr = bytearray(command)
            self._serial.write(byteStr)
        elif demo == "max":
            command = [rc.OpCode.demo]
            byteStr = bytearray(command)
            self._serial.write(byteStr)
        elif demo == "spot":
            command = [rc.OpCode.spot]
            byteStr = bytearray(command)
            self._serial.write(byteStr)
        elif demo == "dock":
            command = [rc.OpCode.coverAndDock]
            byteStr = bytearray(command)
            self._serial.write(byteStr)
        else:
            return False
        return True
    
    #def demo(self):
    #    #if (data == -1):
    #     #   data = 255
    #    command = [rc.OpCode.max_clean]
    ##    #print(command)
     #   byteStr = bytearray(command)
    #    self._serial.write(byteStr)

    #def cover(self):
    #    command = [rc.OpCode.clean]
    #    #print(command)
    #    byteStr = bytearray(command)
    #    self._serial.write(byteStr)

    #def coverAndDock(self):
    #    command = [rc.OpCode.coverAndDock]
    #    #print(command)
    #    byteStr = bytearray(command)
    #    self._serial.write(byteStr)

    #def spot(self):
    #    command = [rc.OpCode.spot]
    #    #print(command)
    #    byteStr = bytearray(command)
    #    self._serial.write(byteStr)

    # Set robot mode
    def setMode(self, mode):
        if mode == "off":
            command = [rc.OpCode.stop]
        elif mode == "passive":
            command = [rc.OpCode.start]
        elif mode == "safe":
            command = [rc.OpCode.safe]
        elif mode == "full":
            command = [rc.OpCode.full]        
        byteStr = bytearray(command)
        return self._serial.write(byteStr)

    # Get robot mode
    # REMOVE THIS
    def getMode(self):
        strCmd = QueryCommand()
        strCmd.addPacket(spf.PacketId.oiMode)
        self.send(strCmd)
        self.checkStream()
        return self._streamPacketFactory.getRobotState()._mode._oiMode

    # Send power 
    def powerCommand(self, command):
        if command == "start":
            command = [rc.OpCode.start]
        elif command == "stop":
            command = [rc.OpCode.power]
        elif command == "reset":
            command = [rc.OpCode.reset]
        byteStr = bytearray(command)
        self._serial.write(byteStr)
