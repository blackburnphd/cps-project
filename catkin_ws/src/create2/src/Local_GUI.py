#!/usr/bin/env python
import Tkinter as tk
import GUIBase as gb
import GUI_Client as gc
import time

if __name__ == "__main__":
    guiClient = gc.GUI_Client("gui_client")
    root = tk.Tk()
    app = gb.ClientApp(guiClient, master=root)

    # Overrides "x" button on the window
    root.protocol('WM_DELETE_WINDOW', app.close)
    root.mainloop()
