import SerialComponent as ser
import StreamPacketFactory as spf
import time
import abc

class OpCode:
    # start commands
    start = 128 # Sets mode to Passive
    reset = 7   # Sets mode to off
    stop = 173  # Sets mode to off and stops the OI
    baud = 129
    safe = 131
    full = 132
    power = 133 # Powers down Roomba. OI needs to be in Passive, Safe, or Full. Changes mode to Passive
    # demo commands
    demo = 136          # Max clean
    cover = 135         # Default cleaning cycle
    coverAndDock = 143  # Seek dock
    spot = 134          # Spot clean
    # actuator commands
    drive = 137
    driveDirect = 145
    leds = 139
    digitalOutputs = 147
    pwmLowSideDrivers = 144
    lowSideDrivers = 138
    sendIr = 151
    song = 140
    playSong = 141
    # input commands
    sensors = 142
    queryList = 149
    stream = 148
    pauseResumeStream = 150
    # script commands
    script = 152
    playScript = 153
    showScript = 154
    # wait commands
    waitTime = 155
    waitDistance = 156
    waitAngle = 157
    waitEvent = 158
