class RobotProxy:
    def __init__(self, robotState, robotCommand):
        self._robotState = robotState
        self._robotCommand = robotCommand
        self._subscriberList = []

    def connect(self):
        self._robotCommand.connect()

    def disconnect(self):
        self._robotCommand.disconnect()

    def startStream(self):
        self._robotCommand.startStream()

    def checkStream(self):
        self._robotCommand.checkStream()

    # whichLED bit field specifies which LEDS to turn on:
    # bit 2 = play, bit 3 = advance (green, full intensity)
    # whichLED = (0x1 << 1) (play on)
    # whichLED = (0x1 << 3) (advance on)
    # whichLED = (0x1 << 1) | (0x1 << 3) play and advance on
    # power LED color: range (0-255), 0 = green, 255 = red
    # power LED intensity: range (0-255), 0 = off, 255 = full intensity
    def setLEDs(self, whichLED, powerColor, powerIntensity):
        self._robotCommand.setLEDs(whichLED, powerColor, powerIntensity)

    # velocity in mm/sec, range -500 to 500 mm/s.
    # radius in mm, range -2000 to 2000 mm.
    def drive(self, velocity, radius):
        self._robotCommand.drive(velocity, radius)

    # velocity in mm/sec, range -500 to 500 mm/s.
    def driveDirect(self, rightWheelVelocity, leftWheelVelocity):
        self._robotCommand.driveDirect(rightWheelVelocity, leftWheelVelocity)

    def start(self):	#added for Create 2
        self._robotCommand.start()

    # Set robot mode
    def setMode(self, mode):
        return self._robotCommand.setMode(mode)

    # Get robot mode
    def getMode(self):
        return self._robotCommand.getMode()

    # Send power command
    def powerCommand(self, command):
        self._robotCommand.powerCommand(command)

    def cleanCommand(self, demo):
        return self._robotCommand.cleanCommand(demo)

    def registerSubscriber(self, subscriber):
        pass

    def notify(self):
        pass

    def setSerialPortInfo(self, portString):
        self._robotCommand.setSerialPortInfo(portString)

    def getRobotState(self):
        return self._robotState
