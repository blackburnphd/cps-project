# all of the RobotState fields with units/range at a glance:
# units and ranges taken from
# http://www.irobot.com/filelibrary/pdfs/hrd/create/create%20open%20interface_v2.pdf
#
# robotState._wheelDrop._caster (1 = drop, 0 = no drop)
# robotState._wheelDrop._left (1 = drop, 0 = no drop)
# robotState._wheelDrop._right (1 = drop, 0 = no drop)
# robotState._bump._left (1 = bump, 0 = no bump)
# robotState._bump._right (1 = bump, 0 = no bump)
# robotState._cliff._left (1 = cliff, 0 = no cliff)
# robotState._cliff._right (1 = cliff, 0 = no cliff)
# robotState._cliff._frontLeft (1 = cliff, 0 = no cliff)
# robotState._cliff._frontRight (1 = cliff, 0 = no cliff)
# robotState._battery._charge (0 to 65535 mAh)
# robotState._battery._capacity (0 to 65535 mAh)
# robotState._wheelVelocity._left (-500 to 500 mm/s)
# robotState._wheelVelocity._right (-500 to 500 mm/s)
# robotState._wheelVelocity._overall (-500 to 500 mm/s)
        
class WheelVelocityState:
    def __init__(self):
        self._left = 0
        self._right = 0
        self._overall = 0

class WheelDropState:
    def __init__(self):
        self._caster = 0
        self._left = 0
        self._right = 0

class CliffState:
    def __init__(self):
        self._left = 0
        self._right = 0
        self._frontLeft = 0
        self._frontRight = 0

class BumpState:
    def __init__(self):
        self._left = 0
        self._right = 0

class BatteryState:
    def __init__(self):
        self._capacity = 0
        self._charge = 0
        self._chargingState = 0

class ModeState:
    def __init__(self):
        self._oiMode = 0

class RobotState:
    def __init__(self):
        self.reset()

    def reset(self):
        self._mode = ModeState()
        self._wheelVelocity = WheelVelocityState()
        self._wheelDrop = WheelDropState()
        self._cliff = CliffState()
        self._bump = BumpState()
        self._battery = BatteryState()
