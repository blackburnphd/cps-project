# tested with PySerial 2.7
import serial

class SerialComponent:

    def __init__(self):
        self._isOpen = False
        self._portString = ""
        self._handle = serial.Serial(baudrate=115200 ,
            bytesize=8,
            stopbits=serial.STOPBITS_ONE,
            xonxoff=False,
            timeout=0) # seconds

    def open(self, portString):
        if (self._handle.isOpen()):
            port = self._handle.port
            print("Socket is already open: %s"%port)
        else:
            self._portString = portString
            #self._handle = serial.Serial(port="/dev/ttyUSB0",
            #self._handle = serial.Serial(port="/dev/ttyS0",
            print("Opening serial port with port:%s"%(portString))
            self._handle.port = portString
            self._handle.open()
            if not self._handle.isOpen():
                print("Port %s was not successfully opened"%portString)
                return False
        return True

    def ensureOpened(self):
        if not self._handle.isOpen():
            if not self.open(self._portString):
                return False
        return True

    def close(self):
        if (self._handle.isOpen()):
            print("Closing socket")
            self._handle.close()
        else:
            print("Socket is already closed")
       
    def write(self, byteSequence):
        if not self.ensureOpened():
            return False
        wbytes = self._handle.write(bytes(byteSequence))
        #print("write ", wbytes, " number of bytes:", byteSequence)
        blist = list(byteSequence)
        return True

    def tryRead(self, numBytes=1024):
        return self._handle.read(numBytes)

    def blockRead(self, numBytes=1024):
        messageList = []
        val = self.tryRead(numBytes)
        messageList.append(val)
        totalRead = len(val)
        while (totalRead < numBytes):
            moreBytes = self.tryRead(numBytes-totalRead)
            readCount = len(moreBytes)
            if (readCount > 0):
                messageList.append(moreBytes)
                totalRead += readCount
        return b"".join(messageList)

    def read(self, numBytes=1024):
        if not self.ensureOpened():
            return False
        val = self._handle.read(numBytes)
        if (len(val) == 0):
            print("serial read returned nil")
        #else:
        #    print("serial read returned %d bytes (%d ...)" % (len(val), int(val[0])))
        return val

