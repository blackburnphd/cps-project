#!/usr/bin/env python
import rospy
from create2.srv import *
import LocalRobotProxy as lrp
from std_msgs.msg import String
from create2.msg import *
from threading import Thread

class Serial_Server:
    def __init__(self, robotProxy):
        self._connectFlag = 0
        self._robotProxy = robotProxy
        rospy.init_node('serial_server')
        self.robotStateThread = Thread(target = self.robotState_topic)
        self.run_thread = True

        # Robot Services
        connect = rospy.Service('connect', Connect, self.connect_service)
        startStream = rospy.Service('mode', Mode, self.mode_service)
        drive = rospy.Service('drive', Drive, self.drive_service)
        setLEDs = rospy.Service('setLEDs', SetLEDs, self.setLEDs_service)
        demo = rospy.Service('demo', Demo, self.demo_service)
        driveDirect = rospy.Service('driveDirect', DriveDirect, self.driveDirect_service)
        test = rospy.Service('testService', Test, self.testSend_service)
        setMode = rospy.Service('setMode', SetMode, self.setMode_service)
        powerCommand = rospy.Service('powerCommand', PowerCommand, self.powerCommand_service)
        abort = rospy.Service('abort', Abort, self.abort_service)

        # Robot State Topic
        self.robotState_pub = rospy.Publisher('robotState', IntList, queue_size=10)

        rospy.loginfo("Ready to provide serial services")
        rospy.spin()
        rospy.loginfo("Shutting down Serial Server node")

    def connect_service(self, req):
        if self._connectFlag:
            rospy.loginfo("GUI is already using port %s"%(req.serialPortVar))
        else:        
            serialPortVar = req.serialPortVar
            rospy.loginfo("Received connect service query: %s"%(serialPortVar))
            if serialPortVar == 0:
                #self._robotProxy.stopStream()
                #self._robotProxy.disconnect()
                return ConnectResponse()
            else:
                self._robotProxy.setSerialPortInfo(serialPortVar)
                self._robotProxy.connect()
                self._robotProxy.start()
                self._robotProxy.startStream()
                # Continuously publish topics
                if not self.robotStateThread.isAlive():
                    self.run_thread = True
                    self.robotStateThread.start()
                #robotStateThread.join()
                return ConnectResponse()

    # Parse robotState information and publish a message
    def robotState_topic(self):
        updateRate = rospy.Rate(15)
        while (not rospy.is_shutdown()) and self.run_thread:
            updateRate.sleep()
            # Update robot state
            self._robotProxy.checkStream()
            rs = self._robotProxy.getRobotState()
            
            # Aggregate data into arrays
            wheelVelocity = [rs._wheelVelocity._left, rs._wheelVelocity._right, rs._wheelVelocity._overall]
            wheelDrop = [rs._wheelDrop._caster, rs._wheelDrop._left, rs._wheelDrop._right]
            cliffState = [rs._cliff._left, rs._cliff._right, rs._cliff._frontLeft, rs._cliff._frontRight]
            bumpState = [rs._bump._left, rs._bump._right]
            battery = [rs._battery._capacity, rs._battery._charge, rs._battery._chargingState]
            status = [rs._mode._oiMode]

            # Compile data name and data arrays Winto a list
            statsList = [
                ["Velocity", wheelVelocity],
                ["Battery", battery],
                ["Mode", status]
            ]

            # Create and publish messages
            msg = IntList()
            for stat in statsList:
                msg.name = stat[0]
                msg.data = stat[1]
                self.robotState_pub.publish(msg)

    def mode_service(self, req):
        mode = req.mode
        rospy.loginfo("Received mode service query: %s"%(mode))
        self._robotProxy.setMode(mode)
        return ModeResponse()

    def setLEDs_service(self, req):
        whichLED = req.whichLED
        color = req.color
        intensity = req.intensity
        rospy.loginfo("Received setLEDs service query: (%s,%s,%s)"%(whichLED, color, intensity))
        self._robotProxy.setLEDs(whichLED, color, intensity)
        return SetLEDsResponse()

    def drive_service(self, req):
        velocity = req.velocity
        radius = req.radius
        rospy.loginfo("Received drive service query: (%s,%s)"%(velocity, radius))
        self._robotProxy.drive(velocity, radius)
        return DriveResponse()

    def demo_service(self, req):
        demoType = req.demoType
        rospy.loginfo("Received demo service query: (%s)"%(demoType))
        if self._robotProxy.cleanCommand(demoType):
            return DemoResponse()
        else:
            rospy.logerror("Invalid demo type specified: %s"%(demoType))

    def driveDirect_service(self, req):
        rightVelocity = req.rightVelocity
        leftVelocity = req.leftVelocity
        rospy.loginfo("Received driveDirect service query: (%s,%s)"%(rightVelocity, leftVelocity))
        self._robotProxy.driveDirect(rightVelocity, leftVelocity)
        return DriveDirectResponse()
        
    def testSend_service(self, req):
        rospy.loginfo("Received test service query: %s"%(req.a))
        return TestResponse(req.a)

    # Set mode service call
    def setMode_service(self, req):
        mode = req.mode
        rospy.loginfo("Received mode service query: (%s)"%(mode))
        success = self._robotProxy.setMode(mode)
        return SetModeResponse(success)

    # Power Command service call
    def powerCommand_service(self, req):
        command = req.command
        rospy.loginfo("Received power command service query: (%s)"%(command))
        self._robotProxy.powerCommand(command)
        return PowerCommandResponse()

    # Abort service call
    def abort_service(self, req):
        rospy.loginfo("Received abort service query")
        self._robotProxy.start()
        return AbortResponse()

if __name__ == "__main__":
    robotProxy = lrp.LocalRobotProxy()
    serial_server = Serial_Server(robotProxy)
