#!/usr/bin/env python
import rospy

class logSender:
    def __init__(self):
        rospy.init_node("logSender", log_level=rospy.DEBUG)
        print("Initted node")
        rate = rospy.Rate(10)
        while not rospy.is_shutdown():
            rospy.loginfo(rospy.get_caller_id() + ": Info")
            rospy.logdebug(rospy.get_caller_id() + ": Debug")
            rospy.logwarn(rospy.get_caller_id() + ": Warn")
            rospy.logerr(rospy.get_caller_id() + ": Error")
            rospy.logfatal(rospy.get_caller_id() + ": Fatal")
            rate.sleep()

if __name__ == '__main__':
    print("1")
    logSender = logSender()
