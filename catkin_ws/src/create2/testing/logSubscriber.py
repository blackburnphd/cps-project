#!/usr/bin/env python
import rospy
from rosgraph_msgs.msg import Log

# This node will subscribe to the rosout topic. All ROS logs are sent through the rosout topic
class logSubscriber:
    def __init__(self):
        rospy.init_node("logSubscriber")
        rospy.Subscriber("/rosout_agg", Log, self.callback)
        print("Ready to subscribe")
        rospy.spin()
    def callback(self, msg):
	    print("Hearing message from node %s at level %s:\nMessage: %s\nFile: %s\nFunction: %s"%(msg.name, msg.level, msg.msg, msg.file, msg.function))

if __name__ == '__main__':
    logSubscriber = logSubscriber()
	
#Log information
# http://docs.ros.org/api/rosgraph_msgs/html/msg/Log.html
# rosgraph_msgs
# http://wiki.ros.org/rosgraph_msgs
# Writing a pub / sub
# http://wiki.ros.org/rospy_tutorials/Tutorials/WritingPublisherSubscriber
