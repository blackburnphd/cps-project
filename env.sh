#!/bin/sh

export ROS_IP=$(hostname -I)
export ROS_MASTER_URI=http://jhuber:11311

exec "$@"
